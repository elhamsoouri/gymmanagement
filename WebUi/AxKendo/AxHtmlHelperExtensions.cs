﻿using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using Framework.Tools;

namespace WebUi.AxKendo
{
    public static class AxHtmlHelperExtensions
    {
        public static GridBuilder<T> AxGrid<T>(this HtmlHelper helper, string name, string controller, string action, object routeValues = null) where T : class
        {
            var setting = new AxGridSetting { CaseCadeFilter = true, GridFilterMode = GridFilterMode.Row };
            return helper.Kendo().Grid<T>()
                .Name(name)
                .Excel(excel => excel.FileName(name + "_" + DateTime.Now.ToPerDateTimeString("yyyy/MM/dd HH:mm:ss")).AllPages(true))
                .Pdf(pdf => pdf.FileName(name + "_" + DateTime.Now.ToPerDateTimeString("yyyy/MM/dd HH:mm:ss")).AllPages())
                .AllowCopy(false).Navigatable(x => x.Enabled(true))
                .HtmlAttributes(new { style = "font: 11px tahoma", @class = "unselectable" })
                .Groupable(x => x.Enabled(false))
                .Resizable(resize => resize.Columns(true))
                .Editable(editable => editable.Enabled(false)).NoRecords("داده ای برای نمایش وجود ندارد")
                .Filterable(f => f.Mode(setting.GridFilterMode).Operators(operators => operators.ForString(str => str.Clear().Contains("شامل").IsEqualTo("برابر باشد با").IsNotEqualTo("برابر نباشد با").StartsWith("شروع شود با").DoesNotContain("شامل نباشد")).ForNumber(x => x.IsEqualTo("برابر با").IsGreaterThan("بزرگ تر از").IsGreaterThanOrEqualTo("بزرگ تر مساوی ").IsLessThan("کمتر از ").IsLessThanOrEqualTo("کمتر مساوی").IsNotEqualTo("مساوی نباشد")).ForDate(t => t.IsEqualTo("برابر با").IsGreaterThan("بزرگ تر از").IsGreaterThanOrEqualTo("بزرگ تر مساوی").IsLessThan("کوچتر از").IsLessThanOrEqualTo("کوچکتر مساوی").IsNotEqualTo("برابر نباشد")).ForEnums(e => e.IsEqualTo("برابر با").IsNotEqualTo("برابر نباشد با"))).Messages(msg => msg.Clear("خالی").Filter("اعمال").Info("نحوه ی جستجو")))
                .Pageable(pageable => pageable.Refresh(true).Messages(messages => messages.Display("نمایش رکورد های  {0} تا {1}، تعداد کل رکورد ها: {2}").AllPages("همه").First("صفحه اول").Last("صفحه آخر").Refresh("بروزرسانی").Empty("داده ای وجود ندارد").ItemsPerPage("در صفحه").Next("صفحه بعد").MorePages("صفحات بعدی")).PageSizes(new[] { "5", "10", "15", "20", "All" }).ButtonCount(3).PreviousNext(true)).Selectable().NoRecords("داده ای وجود ندارد").Pageable()
                .DataSource(dataSource => dataSource
                    .Ajax()
                    .Read(action, controller, routeValues));
        }

        public static MvcHtmlString AxLabelFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);
            var labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();

            if (string.IsNullOrEmpty(labelText))
            {
                return MvcHtmlString.Empty;
            }

            var tag = new TagBuilder("label");
            tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));
            if (metadata.IsRequired)
            {
                var span = new TagBuilder("span");
                span.SetInnerText("*");
                span.AddCssClass("required");
                tag.InnerHtml = labelText + "\r\n" + span.ToString(TagRenderMode.Normal);
            }
            else
                tag.SetInnerText(labelText);

            tag.AddCssClass("control-label col-md-12");
            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
        public class AxGridSetting
        {
            public AxGridSetting()
            {
                GridFilterMode = GridFilterMode.Row;
                CaseCadeFilter = true;
                SelectionMode = GridSelectionMode.Single;
                SortMode = GridSortMode.SingleColumn;
            }
            public GridFilterMode GridFilterMode { get; set; }
            public bool CaseCadeFilter { get; set; }
            public GridSelectionMode SelectionMode { get; set; }
            public GridSortMode SortMode { get; set; }
        }

        public static GridBoundColumnBuilder<TModel> NumericColumn<TModel, TValue>(this GridColumnFactory<TModel> column, Expression<Func<TModel, TValue>> expression, bool isMultiFilter = false) where TModel : class
        {
            return column.Bound(expression).HtmlAttributes(new { @class = "text-left-center" }).Filterable(x => x.Cell(cell => cell.ShowOperators(true)).Multi(isMultiFilter).Messages(t => t.Search("جستجو").CheckAll("انتخاب همه").SelectedItemsFormat("{0} مورد انتخاب شده").Cancel("انصراف")).Search(true));
        }
        public static GridBoundColumnBuilder<TModel> DateTimeColumn<TModel, TValue>(this GridColumnFactory<TModel> column, Expression<Func<TModel, TValue>> expression, bool isMultiFilter = false) where TModel : class
        {
            return column.Bound(expression).HtmlAttributes(new { @class = "text-left-center" }).Filterable(ftb => ftb.Extra(false).Multi(isMultiFilter).Search(true).Messages(x => x.Search("جستجو").CheckAll("انتخاب همه").SelectedItemsFormat("{0} مورد انتخاب شده").Cancel("انصراف")).Cell(cell => cell.Template("filterPanel").ShowOperators(false).Operator("contains")));
        }
        public static GridBoundColumnBuilder<TModel> StringColumn<TModel, TValue>(this GridColumnFactory<TModel> column, Expression<Func<TModel, TValue>> expression, bool isMultiFilter = false) where TModel : class
        {
            return column.Bound(expression).HtmlAttributes(new { @style = "text-align: center;" }).Filterable(ftb => ftb.Cell(cell => cell.Operator("contains")).Extra(false).Multi(isMultiFilter).Search(true).Messages(x => x.Search("جستجو").CheckAll("انتخاب همه").SelectedItemsFormat("{0} مورد انتخاب شده").Cancel("انصراف")));
        }
        public static GridBoundColumnBuilder<TModel> BooleanColumn<TModel, TValue>(this GridColumnFactory<TModel> column, Expression<Func<TModel, TValue>> expression, string trueValue = "فعال", string falseValue = "غیر فعال", bool isMultiFilter = false) where TModel : class
        {
            var fieldName = ReflectionTools.GetName(expression);
            return column.Bound(expression).HtmlAttributes(new { @style = "text-align: center;" }).ClientTemplate("#= " + fieldName + " ? '" + trueValue + "' : '" + falseValue + "' #").Filterable(ftb => ftb.Cell(cell => cell.Operator("eq")).Messages(x => x.IsFalse(falseValue).IsTrue(trueValue).Filter("فیلتر")));
        }

        public static GridBoundColumnBuilder<TModel> PersianDateTimeColumn<TModel, TValue>(this GridColumnFactory<TModel> column, Expression<Func<TModel, TValue>> expression, string dateFormat = "YYYY/MM/DD HH:MM:ss", bool isMultiFilter = false) where TModel : class
        {
            var fieldName = ReflectionTools.GetName(expression);
            return column.Bound(expression).HtmlAttributes(new { @class = "text-left-center" }).ClientTemplate("#=toPersianDate(" + fieldName + ",'" + dateFormat + "')#").Filterable(false);
        }
    }

}
