﻿using System;
using System.Collections.Generic;

namespace WebUi
{
    public static class FileCache
    {
        public static List<UploadFile> FileCacheList;

        public static void Add(UploadFile file)
        {
            if (FileCacheList == null)
                FileCacheList = new List<UploadFile>();
            FileCacheList.Add(file);
        }
    }

    public class UploadFile
    {
        public Guid Id { get; set; }
        public byte[] FileContent { get; set; }
        public DateTime InsertTime { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public int Size { get; set; }
    }
}