﻿using System.Threading.Tasks;
using Microsoft.Owin;

namespace WebUi.Middlewares
{
    public class CookieMiddleware : OwinMiddleware
    {
        private readonly OwinMiddleware _next;

        public CookieMiddleware(OwinMiddleware next) : base(next)
        {
            _next = next;
        }

        public override Task Invoke(IOwinContext context)
        {
            var cookie = (string)context.Request.Cookies["AxToken"];

            context.Request.Headers["Authorization"] = "bearer " + cookie;
            return _next.Invoke(context);
        }
    }
}