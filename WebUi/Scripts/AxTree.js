﻿
(function ($) {

    $.fn.axTree = function (options) {
        var element = this;

        //element.jstree('refresh');
        var settings = $.extend({
            url: null,
            search: null,
            checkbox: false,
            icon: false,
            EntireLoad: false,
            insertCallBack: null,
            updateCallBack: null,
            deleteCallBack: null,
            selectCallBack: null,
            dblCallBack:null,
            contextMenu: null,
            massload: null
        }, options);

        if (settings.contextMenu == true) {
            settings.contextMenu = function (o, cb) {
                var items;
                if (settings.checkbox) {
                    items = {
                        changeState: {
                            separator_after: true,
                            label: "تغییر وضعیت",
                            action: function (data) {

                                var inst = $.jstree.reference(data.reference);
                                obj = inst.get_node(data.reference);
                                if (data.reference.hasClass("jstree-clicked")) {
                                    element.jstree('checkgroup_node', obj.id);
                                    element.jstree('open_node', obj.id);
                                }
                                else if (data.reference.hasClass("jstree-checkgroup")) {
                                    element.jstree('deselect_node', obj.id);
                                    element.jstree('open_node', obj.id);
                                }
                                else {
                                    element.jstree('select_node', obj.id);
                                    element.jstree('open_node', obj.id);
                                }
                            },
                            icon: "/Content/images/check.png"
                        },
                        selectAllItem: {
                            separator_after: true,
                            label: "کل شاخه فعال شود",
                            action: function (data) {

                                var inst = $.jstree.reference(data.reference);
                                obj = inst.get_node(data.reference);
                                element.jstree('select_node', obj.id);
                                element.jstree('open_node', obj.id);
                                var children = obj.children_d;
                                element.jstree('select_node', obj.children_d);
                            },
                            icon: "/Content/images/check-all.png"
                        },
                        GroupWay: {
                            separator_after: true,
                            label: "مطابق با گروه های عضو",
                            action: function (data) {

                                var inst = $.jstree.reference(data.reference);
                                obj = inst.get_node(data.reference);
                                element.jstree('checkgroup_node', obj.id);
                                element.jstree('open_node', obj.id);
                                var children = obj.children_d;
                                element.jstree('checkgroup_node', obj.children_d);
                            },
                            icon: "/Content/images/Group.png"
                        },
                        deselectAllItem: {
                            label: "کل شاخه غیر فعال شود",
                            action: function (data) {

                                var inst = $.jstree.reference(data.reference);
                                obj = inst.get_node(data.reference);
                                element.jstree('open_node', obj.id);
                                element.jstree('deselect_node', obj.id);
                            },
                            icon: "/Content/images/uncheck.png"
                        }
                    };
                }
                else {
                    items = $.jstree.defaults.contextmenu.items();
                    items["ccp"] = false;
                    items["create"].label = "ایجاد زیر شاخه";
                    items["rename"].label = "تغییر نام";
                    items["remove"].label = "حذف";
                }
                return items;
            }
        }

       
        var plugins = ["unique", "ui"];
        if (settings.contextMenu)
            plugins.push("contextmenu");
        if (settings.search)
            plugins.push("search");
        if (settings.checkbox)
            plugins.push("checkbox");
        if (settings.massload)
            plugins.push("massload");

        element.jstree({
            "checkbox": {
                'keep_selected_style': false,
                'three_state': false,
                'cascade': '',
                'real_checkboxes': false,
                'tie_selection': true,
            },
            "core": {
                'check_callback': function (op, node) {
                    if (op === 'delete_node') {
                        if (node.state.loaded && node.children.length == 0) {
                            var r = confirm("آیا از حذف اطمینان دارید؟");
                            if (r == true) {
                                settings.deleteCallBack(node.id);
                            } else {
                                return false;
                            }
                        }
                        else { toastr.error('حذف گره پدر امکان پذیر نیست'); return false; }
                    }
                },
                "massload": {
                    "url": settings.url,
                    "data": function (nodes) {
                        return { "ids": nodes.join(",") };
                    }
                },
                'strings': { 'Loading ...': 'در حال بارگذاری ...' },
                "multiple": true,
                'data': {
                    'url': settings.url,
                    "type": "GET",
                    "dataType": "json",
                    "contentType": "application/json; charset=utf8",
                    'data': function (node) {

                        return { 'id': node.id };
                    }
                },
                "themes": {
                    "variant": "large",
                    "icons": settings.icon
                }
            },
            "plugins": plugins,
            "search": {
                'case_sensitive': false,
                'show_only_matches': true
            },
            "contextmenu": {
                "items": settings.contextMenu,
                "select_node": false
            }
        })
           .on('create_node.jstree', function (e, data) {
               settings.insertCallBack(data.node.text, data.node.parent, data);
           })
           .on('rename_node.jstree', function (e, data) {
               settings.updateCallBack(data.node.id, data.node.text, data.node.parent);
           })
           .on('dblclick.jstree', function (e) {
               if (settings.dblCallBack != null) {
                   var node = $(e.target).closest("li");
                   settings.dblCallBack(node[0].id, e.target.text);
               }
               else {
                   var node = $(e.target).closest("li");
                   parent.CityId = node[0].id;
                   parent.CityTitle = e.target.text;
               }

               //window.parent.$(".k-window-iframecontent").data("kendoWindow").close();
           }).on('loaded.jstree', function (e, data) {
               debugger
               if (settings.EntireLoad) {
                   element.jstree('open_all');
                   element.bind("ready.jstree", function () {
                       element.jstree('close_all');
                   }).jstree();
               } else {
                   // element.jstree('open_all');
               }
           }).on("select_node.jstree", function (e, data) {

               if (settings.selectCallBack != null) {
                   settings.selectCallBack(data.node.id, data.node.text);
               }
               else if (settings.checkbox) {
                   element.jstree('open_node', data.node);
                   ParentNode = element.jstree('get_parent', data.node);
                   element.jstree('select_node', ParentNode);
               }
           }).on("deselect_node.jstree", function (e, data) {
               if (settings.checkbox) {
                   element.jstree('open_node', data.node); //need this to have it deselect hidden nodes
                   ChildrenNodes = jQuery.makeArray(element.jstree('get_children_dom', data.node));
                   element.jstree('deselect_node', ChildrenNodes);
                   // element.jstree('close_node', data.node);
               }
           }).on('changed.jstree', function (e, data) {
               //if (settings.checkbox)
                   //setDirty();
               //  data.node.a_attr.class = "CloseClass";
               //var i, j, r = [];
               //for (i = 0, j = data.selected.length; i < j; i++) {
               //    r.push(data.instance.get_node(data.selected[i]).text);
               //}
               //$('#event_result').html('Selected: ' + r.join(', '));
           });
       
    };

}(jQuery));
