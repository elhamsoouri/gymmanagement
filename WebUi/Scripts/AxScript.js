﻿
function GetSelectedGridRow(gridId) {

    var grid = $("#" + gridId).data("kendoGrid");
    var selectedItem = grid.dataItem(grid.select());
    if (!selectedItem) {
        toastr.info("لطفا یک سطر را انتخاب نمائید");
        return;
    }
    return selectedItem;
}

function ChangeUrl(url) {

    window.location.href = url;
}

function ConfirmAlert(yesCallback, noCallback, body) {
    if (!body)
        body = "آیا از حذف این رکورد اطمینان دارید؟";
    $.confirm({
        content: body,
        buttons: {
            OK: {
                text: 'بلی',
                btnClass: 'btn-red width-20',
                keys: ['enter'],
                action: function () {
                    yesCallback();
                }
            },
            Cancel: {
                text: 'خیر',
                btnClass: 'width-20',
                keys: ['esc'],
                action: function () {
                    if (noCallback)
                        noCallback();
                    else close();
                }
            }
        },
        draggable: true,
        type: 'orange'
    });
}

function Delete(url, callback) {
    $.ajax({
        url: url,
        type: "POST",
        success: function (result) {
            var objResult = ToObjectResult(result);
            if (objResult.Type === 1) {
                callback();
            }
            toastr.toast(objResult.Msg, objResult.Type);
        }
    });
}

function Save(url, json, callback) {
    $.ajax({
        url: url,
        type: "POST",
        data: json,
        success: function (result) {
            var objResult = ToObjectResult(result);
            if (objResult.Type === 1) {
                toastr.toast(objResult.Msg, objResult.Type);
                console.log(objResult.Msg);
                callback(result);
            } else {
                if (objResult.Type === 2) {
                    toastr.toast(objResult.Msg, objResult.Type);
                    console.log(objResult.Msg);

                }
                if (objResult.Type === 5) {
                    var lst = objResult.Entity;
                    $(".error-validation").remove();
                    $(".border-validation").removeClass("border-validation");
                    for (var i = 0; i < lst.length; i++) {
                        var id = "#Entity_" + lst[i].PropertyName;
                        var role = $(id).attr("data-role");
                        var div = "<div title='" + lst[i].ErrorMessage + "' class='error-validation'></div>";
                        switch (role) {
                            case "dropdownlist":
                                {
                                    $(id).parent().parent().next().html(div);
                                    $(id).siblings(".k-dropdown-wrap").first().addClass("border-validation");
                                    break;
                                }
                            case "numerictextbox":
                                {
                                    $(id).parent().parent().parent().next().html(div);
                                    $(id).parent().addClass("border-validation");
                                    break;
                                }
                            case "maskedtextbox":
                                {
                                    $(id).parent().parent().parent().next().html(div);
                                    $(id).addClass("border-validation");
                                    break;
                                }
                            case "multiselect":
                                {
                                    $(id).parent().parent().parent().next().html(div);
                                    $(id).parent().addClass("border-validation");
                                    break;
                                }
                            case "dropdowntree":
                            {
                                debugger
                                $(id).parent().parent().next().html(div);
                                $(id).parent().addClass("border-validation");
                                break;
                            }
                            default:
                                {
                                    $(id).parent().next().html(div);
                                    $(id).addClass("border-validation");
                                }
                        }

                    }

                }
            }
        }
    });
}

function RefreshGrid(id) {

    $("#" + id).data("kendoGrid").dataSource.read();
}

function ToObjectResult(input) {
    try {
        if (input) {
            return JSON.parse(input);
        }
        return false;
    } catch (e) {
        return input;
    }
}

function toPersianDate(input, format) {
    if (!format)
        format = 'YYYY/MM/DD HH:MM:ss';
    var date = new Date(input);
    var dayWrapper = new persianDate(date);
    return dayWrapper.toLocale('en').format(format);
}


function SetActiveInput() {
    var myElement = $(":input[type!=hidden]:input[readonly!=readonly]:first");
    //var wnd = $("#MyWnd").data("kendoWindow");
    //if (wnd.options.visible) {
    //    myElement = my$("#MyWnd div>:input[type!=hidden]:input[readonly!=readonly]:first");
    //}
    if (myElement != null) {
        myElement.focus();
        var ddl = myElement.data("kendoDropDownList");
        if (ddl) ddl.focus();
    }
}


function openWindow(title, url, id, options) {
    try {
        if (!id) {
            id = "AxWnd";
            $("#AxWnd").removeClass("ax-dirty");
        }
        var wnd = $("#" + id).data("kendoWindow");
        wnd.title(title);
        if (options != null && (options.width || options.height)) {
            wnd.setOptions({
                width: options.width ? options.width : "50%",
                height: options.height ? options.height : "50%"
            });
        } else {
            wnd.setOptions({
                width: "50%",
                height: "50%"
            });
        }
        wnd.options.content = new Object();
        wnd.options.content.url = url;
        wnd.refresh({ cache: false });

        wnd.center().open();
    }
    catch (err) {
        //sendError(err.message, err.stack);
        console.log(err.message + "#####" + err.stack);
    }
}

function closeWindow(id) {
    if (!id) {
        id = "AxWnd";
    }
    var wnd = $("#" + id).data("kendoWindow");
    wnd.close();
}