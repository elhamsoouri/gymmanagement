﻿using System.Web.Mvc;
using WebUi.Filters;

namespace WebUi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ExceptionHandler());   
            //filters.Add(new AuthorizeAttribute());
            filters.Add(new AxAuthorizeAttribute());
        }
    }
}