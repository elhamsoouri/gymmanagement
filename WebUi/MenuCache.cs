﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Autofac;
using Framework.Services;
using Model.Entities;

namespace WebUi
{
    public static class MenuCache
    {

        private static IEnumerable<Menu> _list;
        public static IEnumerable<Menu> List
        {
            get
            {
                if (_list == null)
                {
                    var menuService = Global.Container.Resolve<IBaseService<Menu>>();
                    var menus = menuService.GetAll(x => x.Active && !x.ShowInPopUp && x.ParentId == null).Include(x => x.Children);
                    _list = menus;

                }
                return _list;
            }
        }
    }
}