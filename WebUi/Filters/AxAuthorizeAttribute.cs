﻿using System.Collections.Generic;
using System.Web.Mvc;
using Framework;
using Framework.Entities;

namespace WebUi.Filters
{
    public class AxAuthorizeAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        public AxOp AxOp { get; set; }
        public AxOp ParentAxOp { get; set; }
        public bool ShowInMenu { get; }

        public StateType StateType { get; set; }

        public AxAuthorizeAttribute(AxOp axOp, AxOp parentAxOp, bool showInMenu = false)
        {
            AxOp = axOp;
            ParentAxOp = parentAxOp;
            ShowInMenu = showInMenu;
        }

        public AxAuthorizeAttribute(AxOp axOp, bool showInMenu = false)
        {
            AxOp = axOp;
            ShowInMenu  = showInMenu;
        }



        public AxAuthorizeAttribute()
        {
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var context = filterContext.HttpContext;
            var userIdStr = context.User.Identity.Name;
            var keys = new HashSet<string>();
            var url = context.Request.RawUrl;

            if (!string.IsNullOrWhiteSpace(userIdStr) && StateType != StateType.Ignore)
            {
                //filterContext.Result = new RedirectResult("~/Account/UnAuthorized");
            }
            else
            {
                if (StateType != StateType.Ignore)
                    filterContext.Result = new RedirectResult("~/Basic/Account/Login");
            }
        }


    }
}