﻿using System;
using System.Linq;
using System.Web.Mvc;
using FluentValidation;
using Framework.Logging;
using Framework.Resources;
using Model.Entities;

namespace WebUi.Filters
{
    public class ExceptionHandler : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;
            var controllerName = filterContext.RouteData.Values["controller"];
            var actionName = filterContext.RouteData.Values["action"];

            var response = filterContext.RequestContext.HttpContext.Response;
            response.BufferOutput = true;
            filterContext.ExceptionHandled = true;

            while (exception.InnerException != null)
            {
                exception = exception.InnerException;
            }
            var res = new ApiResult<dynamic> { Type = ApiResultType.ValidationError };
            if (exception.Message.Contains("conflicted with the REFERENCE constraint"))
            {
                var str = Words.FKConflictedConstraint;
                var tableNameArray = exception.Message.Split(new[] { "table " }, StringSplitOptions.RemoveEmptyEntries);
                var latinTableName = tableNameArray[1].Split(new[] { '"' }, 3)[1].Split('.')[1];
                var translatedTableName = "";// Resources.WebUiWords.ResourceManager.GetString(latinTableName, Resources.WebUiWords.Culture);
                if (string.IsNullOrWhiteSpace(translatedTableName))
                    translatedTableName = latinTableName;
                var output = string.Format(str, translatedTableName);
                output = !string.IsNullOrWhiteSpace(output) ? output : tableNameArray[1].Split(new[] { '"' }, 3)[1].Split('.')[1];
                res.Type = ApiResultType.Error;
                res.Msg = output;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(res);
                response.Write(json);
                AxLogger.Debug(filterContext.Exception, "Error calling {0}.{1}", controllerName, actionName);
                return;
            }
            if (filterContext.Exception is ValidationException validationException)
            {
                var errors = validationException.Errors.Select(x => new { x.PropertyName, x.ErrorMessage, x.Severity });
                res.Entity = errors;
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(res);
                response.Write(json);
                // response.StatusCode = 406;
                AxLogger.Info("ValidationException occurred {0}.{1}\r\n\r\n{2}", controllerName, actionName, filterContext.Exception.Message);
            }
            else
            {
                res.Type = ApiResultType.Error;
                //todo : if(IsDefaultAttribute() develepmet or prod)FileNotFoundException: Could not load file or assembly 'NLog.Web' or one of its dependencies. The system cannot find the file specified.

                //res.Msg = "خطای مدیریت نشده رخ داده است";
                res.Msg = filterContext.Exception.ToString();
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(res);
                response.Write(json);
                AxLogger.Error(filterContext.Exception, "Error calling {0}.{1}", controllerName, actionName);
            }
        }
    }
}