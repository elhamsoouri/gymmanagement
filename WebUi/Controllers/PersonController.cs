﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Resources;
using Framework.Services;
using Framework.Tools;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Model.Entities;
using Model.Enum;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class PersonController : BaseController<Person, PersonViewModel>
    {
        private readonly IBaseService<File> _fileService;

        public PersonController(IBaseService<Person> personService, IBaseService<File> fileService) : base(personService)
        {
            Service = personService;
            _fileService = fileService;
        }

        [AxAuthorize(AxOp.PersonList, true)]
        public ActionResult List()
        {
            return View();
        }

        [AxAuthorize(ParentAxOp = AxOp.PersonList, StateType = StateType.CheckParent)]
        public JsonResult GetPersonList([DataSourceRequest] DataSourceRequest request)
        {
            var personList = Service.GetAll().OrderByDescending(x => x.Id).Select(x => new
            {
                x.Id,
                x.NationalCode,
                FullName = x.FirstName + " " + x.LastName,
                x.IdentityNumber,
                x.FatherName,
                x.Sex,
                x.InsertDateTime
            });
            return Json(personList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AxAuthorize(AxOp.PersonItem)]
        public ActionResult Item(int id = 0)
        {
            PersonViewModel viewModel = new PersonViewModel();
            var model = Service.Find(id);
            if (model != null)
            {
                viewModel.Entity = model;
                viewModel.Birthday = viewModel.Entity.Birthday?.ToPerDateTimeString("yyyy/MM/dd");
                var img = _fileService.GetAll(x => x.PersonId == id && x.FileType == FileEnum.PersonPicture).SingleOrDefault();
                if (img != null)
                    viewModel.PersonPictureId = img.Id;
            }
            return View(viewModel);
        }

        [AxAuthorize(AxOp.PersonInsert)]
        public override ActionResult Insert(PersonViewModel item)
        {
            var entity = item.Entity;
            entity.Birthday = item.Birthday.GetMiladiDate();
            Service.Add(entity);
            var personId = entity.Id;
            var file = FileCache.FileCacheList?.FirstOrDefault(x => x.Id == item.ImageId);
            if (file != null)
            {
                var newFile = new File
                {
                    Title = file.FileName,
                    FileName = file.FileName,
                    FileType = FileEnum.PersonPicture,
                    FileContent = file.FileContent,
                    Size = file.FileContent.Length,
                    PersonId = personId
                };
                _fileService.Add(newFile);
                FileCache.FileCacheList.Remove(file);
            }

            return Json(new ApiResult
            {
                Id = personId
            });
        }

        [AxAuthorize(AxOp.PersonUpdate)]
        public override ActionResult Update(PersonViewModel item)
        {
            var entity = item.Entity;
            entity.Birthday = item.Birthday.GetMiladiDate();
            Service.Update(entity);
            var file = FileCache.FileCacheList?.FirstOrDefault(x => x.Id == item.ImageId);
            if (file == null)
                return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessEdit });
            var oldFile = _fileService.GetAll(x => x.PersonId == item.Entity.Id).FirstOrDefault();
            if (oldFile != null)
            {
                _fileService.Delete(oldFile);
            }
            var newFile = new File
            {
                Title = file.FileName,
                FileName = file.FileName,
                FileType = FileEnum.PersonPicture,
                FileContent = file.FileContent,
                Size = file.FileContent.Length,
                PersonId = entity.Id
            };
            _fileService.Add(newFile);
            FileCache.FileCacheList.Remove(file);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessEdit });
        }

        [AxAuthorize(AxOp.PersonDelete)]
        public override ActionResult Delete(int id)
        {
            var img = _fileService.GetAll(x => x.PersonId == id && x.FileType == FileEnum.PersonPicture).FirstOrDefault();
            if (img != null)
                _fileService.Delete(img.Id);
            return base.Delete(id);
        }

    }
}