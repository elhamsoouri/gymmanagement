﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Services;
using Model.Entities;
using WebUi.Filters;

namespace WebUi.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IBaseService<Menu> _menuService;

        public DashboardController(IBaseService<Menu> menuService)
        {
            _menuService = menuService;
        }

        [AxAuthorize(AxOp.DashboardIndex)]
        public ActionResult Index()
        {
            return View();
        }

        [AxAuthorize(ParentAxOp = AxOp.DashboardIndex, StateType = StateType.CheckParent)]
        public ActionResult Navigation()
        {
            var systemKey = Request.RequestContext.RouteData.Values["systemKey"];
            var opEnum = (AxOp)Enum.Parse(typeof(AxOp), systemKey.ToString());
            var system = _menuService.FirstOrDefault(x => x.AxOp == opEnum);
            if (system == null)
                system = _menuService.FirstOrDefault(x => x.Active && x.AxOp == AxOp.Basic);
            var menus = _menuService.GetAll(x => x.Active && !x.ShowInPopUp && x.ParentId == system.Id).Include(x => x.Children);
            //var user = _userService.GetAll(x => x.Id == 4).FirstOrDefault();
            //if (user != null) ViewBag.User = user.FirstName + " " + user.LastName;
            var a = menus.ToList();
            return PartialView("_Menu", menus);
        }

        [AxAuthorize(ParentAxOp = AxOp.DashboardIndex, StateType = StateType.CheckParent)]
        public ActionResult SystemsMenu()
        {
            var data = _menuService.GetAll(x => x.Active && x.ParentId == null);
            return PartialView("_SystemsMenu", data);
        }
    }
}