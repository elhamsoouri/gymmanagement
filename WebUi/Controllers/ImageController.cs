﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework.Entities;
using Framework.Services;
using Framework.Tools;
using Model.Entities;
using Model.Enum;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class ImageController : BaseController<File, FileViewModel>
    {
        public ImageController(IBaseService<File> service) : base(service)
        {
            Service = service;
        }

        [AxAuthorize(AxOp.PersonImageShow)]
        public ActionResult Show(int id, string guid)
        {
            if (id != 0)
            {
                var imageData = Service.Find(id);
                var mimeType = MimeMapping.GetMimeMapping(imageData.FileName);
                if (imageData.FileContent != null && imageData.FileContent.Length > 0)
                {
                    return File(imageData.FileContent, mimeType, imageData.FileName);
                }
            }

            if (!string.IsNullOrWhiteSpace(guid))
            {
                var pic = FileCache.FileCacheList.FirstOrDefault(x => x.Id.ToString() == guid);
                if (pic?.FileContent != null && pic.FileContent.Length > 0)
                {
                    var mimeType = MimeMapping.GetMimeMapping(pic.FileName);
                    return File(pic.FileContent, mimeType, pic.FileName);
                }
            }
            return null;
        }

        [AxAuthorize(AxOp.PersonImageDelete)]
        [HttpGet]
        public ActionResult DeletePic(int id)
        {
            var image = Service.GetAll(x => x.PersonId == id && x.FileType == FileEnum.PersonPicture).SingleOrDefault();
            if (image != null)
            {
                Service.Delete(image);
            }
            return null;
        }

        [AxAuthorize(AxOp.PersonImageInsert)]
        public ActionResult AddPictureInCache(HttpPostedFileBase image)
        {
            if (image == null) return null;
            var data = StreamHelper.ReadToEnd(image.InputStream);
            var title = System.IO.Path.GetFileName(image.FileName);

            var newFile = new UploadFile
            {
                Id = Guid.NewGuid(),
                FileName = title,
                FileType = FileEnum.PersonPicture.GetDisplayName(),
                FileContent = data,
                Size = image.ContentLength,
                InsertTime = DateTime.Now
            };
            FileCache.Add(newFile);
            return Json(new ApiResult<UploadFile>
            {
                Type = ApiResultType.Success,
                Temp = newFile.Id.ToString()
            });
        }
    }
}