﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Services;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class GeoPosController : BaseController<GeoPos, BaseViewModel<GeoPos>>
    {

        public GeoPosController(IBaseService<GeoPos> service) : base(service)
        {
            Service = service;
        }

        // GET: GeoPos
        //public ActionResult Index()
        //{
        //    return View();
        //}

        [HttpGet]
        [AxAuthorize(ParentAxOp = AxOp.AddressItem,StateType = StateType.CheckParent)]
        public JsonResult GetGeoPosTree(int? id)
        {
            var data = Service.GetAll(x => x.ParentId == id).Include(x => x.Children).ToList()
                .Select(x => new
                {
                    x.Title,
                    id = x.Id,
                    hasChildren = x.Children?.Count > 0,
                });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}