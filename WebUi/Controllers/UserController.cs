﻿using System.Linq;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Resources;
using Framework.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class UserController : BaseController<User, BaseViewModel<User>>
    {
        private readonly IBaseService<Person> _personService;

        public UserController(IBaseService<User> service, IBaseService<Person> personService) : base(service)
        {
            _personService = personService;
            Service = service;
        }

        [AxAuthorize(AxOp.UserList, true)]
        public ActionResult List()
        {
            return View();
        }

        [AxAuthorize(ParentAxOp = AxOp.UserList, StateType = StateType.CheckParent)]
        public JsonResult GetUserList([DataSourceRequest] DataSourceRequest request)
        {
            var userList = Service.GetAll().OrderByDescending(x => x.Id).Select(x => new
            {
                x.Id,
                x.UserName,
                x.InsertDateTime,
                x.IsActive,
                x.IsDeleted
            });
            return Json(userList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AxAuthorize(ParentAxOp = AxOp.GroupItem, StateType = StateType.CheckParent)]
        public JsonResult GetUsersWithPersonalData(string text)
        {

            var userList = Service.GetAll(x => x.UserName.Contains(text) || x.Person.FirstName.Contains(text) || x.Person.LastName.Contains(text)).OrderByDescending(x => x.Id).Select(x => new
            {
                x.Id,
                x.UserName,
                FullName = x.Person.FirstName + x.Person.LastName,
                NationalCode = x.Person.NationalCode,
                PersonalId = x.Person.PersonalId
            });
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        [AxAuthorize(AxOp.UserItem)]
        public ActionResult Item(int id = 0)
        {
            BaseViewModel<User> viewModel = new BaseViewModel<User>();
            var model = Service.Find(id);
            if (model != null)
            {
                viewModel.Entity = model;
            }
            return View(viewModel);
        }

        [AxAuthorize(AxOp.UserInsert)]
        public override ActionResult Insert(BaseViewModel<User> item)
        {
            var entity = item.Entity;
            Service.Add(entity);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessInsert });
        }

        [AxAuthorize(AxOp.UserUpdate)]
        public override ActionResult Update(BaseViewModel<User> item)
        {
            var dbUser = Service.Find(item.Entity.Id);
            dbUser.IsActive = item.Entity.IsActive;
            dbUser.UserName = item.Entity.UserName;
            Service.Update(dbUser);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessEdit });
        }

        [AxAuthorize(AxOp.UserDelete)]
        public override ActionResult Delete(int id)
        {
            var dbUser = Service.Find(id);
            if (dbUser.IsDeleted)
            {
                return Json(new ApiResult { Type = ApiResultType.Error, Msg = "این کاربر قبلا حذف شده است" });
            }
            dbUser.IsDeleted = true;
            Service.Update(dbUser);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessDelete });
        }

        [AxAuthorize(ParentAxOp = AxOp.UserItem, StateType = StateType.CheckParent)]
        public JsonResult GetPersonListForUser(string text)
        {
            var personList = _personService.GetAll(x => x.FirstName.Contains(text) || x.LastName.Contains(text) || x.NationalCode.StartsWith(text)).OrderByDescending(x => x.Id).Select(x => new
            {
                x.Id,
                x.NationalCode,
                FullName = x.FirstName + " " + x.LastName,
                x.PersonalId
            });
            return Json(personList, JsonRequestBehavior.AllowGet);
        }
    }
}