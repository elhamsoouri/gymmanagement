﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class AddressController : BaseController<Address, AddressViewModel>
    {
        public AddressController(IBaseService<Address> service) : base(service)
        {
            Service = service;
        }

        // GET: Address

        [AxAuthorize(AxOp.AddressItem)]
        public ActionResult Item(int personId = 0, int id = 0)
        {
            AddressViewModel viewModel = new AddressViewModel();
            var model = Service.Find(id);
            if (model != null)
                viewModel.Entity = model;
            viewModel.Entity.PersonId = personId;
            return View(viewModel);
        }

        [AxAuthorize(AxOp.AddressList)]
        public ActionResult List(int personId)
        {
            ViewBag.PersonId = personId;
            return View(personId);
        }

        [AxAuthorize(ParentAxOp = AxOp.AddressList, StateType = StateType.CheckParent)]
        public ActionResult GetList([DataSourceRequest]DataSourceRequest request, int? personId)
        {
            var data = Service.GetAll(x => x.PersonId == personId).Include(x => x.GeoPos);
            return Json(data.Select(x => new
            {
                Entity = new
                {
                    x.Id,
                    x.Mobile,
                    x.Email,
                    x.Telephone,
                    x.PostalCode,
                    x.Fax,
                    x.IsMainAddress,
                    x.PostalBox,
                    x.AddressContent
                },
                GeoPosTitle = x.GeoPos.Title,
                x.InsertDateTime
            }).ToList().ToDataSourceResult(request));
        }

        [AxAuthorize(AxOp.AddressInsert)]
        public override ActionResult Insert(AddressViewModel item)
        {
            return base.Insert(item);
        }

        [AxAuthorize(AxOp.AddressUpdate)]
        public override ActionResult Update(AddressViewModel item)
        {
            return base.Update(item);
        }
    }
}