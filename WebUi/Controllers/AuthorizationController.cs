﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Services;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class AuthorizationController : BaseController<Authorization, BaseViewModel<Authorization>>
    {
        private readonly IBaseService<User> _userService;
        private readonly IBaseService<Menu> _menuService;

        public AuthorizationController(IBaseService<Authorization> service, IBaseService<User> userService, IBaseService<Menu> menuService) : base(service)
        {
            _userService = userService;
            _menuService = menuService;
            Service = service;
        }
        // GET: Authorization

        [AxAuthorize(AxOp.OperationsAuthorization, true)]
        public ActionResult Index()
        {
            return View();
        }

        [AxAuthorize(ParentAxOp = AxOp.OperationsAuthorization, StateType = StateType.CheckParent)]
        public ActionResult GetUserGroups(string text)
        {
            var data = _userService.GetAll(x => x.PersonId.ToString().Contains(text) || x.Person.FirstName.Contains(text) || x.Person.LastName.StartsWith(text) || x.UserName.Contains(text)).Select(x => new
            {
                x.UserName,
                FullName = x.Person.FirstName + " " + x.Person.LastName,
                x.Person.NationalCode,
                x.Person.PersonalId,
                x.Id,
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AxAuthorize(ParentAxOp = AxOp.OperationsAuthorization, StateType = StateType.CheckParent)]
        public ActionResult GetMenuTree(int? id)
        {
            var data = _menuService.GetAll(x => x.Active).Include(x=> x.UserPermissions).Include(x => x.Children).ToList();
            var newData = SerializeData(data.Where(x => x.ParentId == null).ToList(), 0);
            return Json(newData, JsonRequestBehavior.AllowGet);
        }

        private ICollection<dynamic> SerializeData(ICollection<Menu> data, int userId)
        {
            //var keys = ((HashSet<string>)Session["UserPermissions"]).ToList();
            return data?.Where(x => x.Active).Select(x => new
            {
                id = x.Key,
                text = x.Title,
                children = SerializeData(x.Children.ToArray(), userId)
            }).ToArray();
        }
    }
}
