﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Resources;
using Framework.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class UserGroupController : BaseController<UserGroup, BaseViewModel<UserGroup>>
    {
        // GET: UserGroup
        private readonly IBaseService<User> _userService;
        private readonly IBaseService<User> _personService;
        public UserGroupController(IBaseService<UserGroup> service, IBaseService<User> userService, IBaseService<Person> personService, IBaseService<User> personService1) : base(service)
        {
            _userService = userService;
            _personService = personService1;
            Service = service;
        }

        [AxAuthorize(AxOp.UserGroupList)]
        public ActionResult List()
        {
            return View();
        }

        [AxAuthorize(ParentAxOp = AxOp.UserGroupList, StateType = StateType.CheckParent)]
        public ActionResult GetList([DataSourceRequest]DataSourceRequest request, int? groupId)
        {
            var data = Service.GetAll(x => x.GroupId == groupId);
            var userData = new List<UserGroupViewModel>();
            foreach (UserGroup userGroup in data)
            {
                var user = _userService.GetAll(x => x.Id == userGroup.UserId).Include(x => x.Person).FirstOrDefault();
                if (user != null)
                    userData.Add(new UserGroupViewModel
                    {
                        Id = userGroup.Id,
                        UserName = user.UserName,
                        FullName = user.Person.FullName,
                        PersonalId = user.Person.PersonalId,
                    });
            }

            return Json(userData.ToList().ToDataSourceResult(request));
        }

        [AxAuthorize(AxOp.UserGroupInsert)]
        [HttpPost]
        public ActionResult InsertUserToGroup(int groupId, int? userId)
        {
            if (userId == null)
            {
                return Json(new ApiResult { Type = ApiResultType.Error, Msg = "لطفا کاربر مورد نظر را انتخاب نمایید" });
            }
            var row = Service.GetAll(x => x.GroupId == groupId && x.UserId == userId).Any();
            if (row)
            {
                return Json(new ApiResult { Type = ApiResultType.Error, Msg = "شخص مورد نظر قبلا در گروه عضو شده است" });
            }
            var userGroup = new UserGroup { GroupId = groupId, UserId = (int)userId };
            Service.Add(userGroup);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessInsert });
        }

        [AxAuthorize(AxOp.UserGroupDelete)]
        [HttpPost]
        public ActionResult DeleteUserGroup(int id)
        {
            Service.Delete(id);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessDelete });
        }
    }
}