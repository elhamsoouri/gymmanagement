﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Framework;
using Framework.Resources;
using Framework.Services;
using Framework.Tools;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;
using WebUi.Provider;

namespace WebUi.Controllers
{
    public class AccountController : BaseController<User, LoginViewModel>
    {
        private readonly IBaseService<LoginLog> _loginLogService;

        public AccountController(IBaseService<User> service, IBaseService<LoginLog> loginLogService) : base(service)
        {
            _loginLogService = loginLogService;
            Service = service;
        }

        [AxAuthorize(StateType = StateType.Ignore)]
        public ActionResult Login()
        {
            return View();
        }

        [AxAuthorize(StateType = StateType.Ignore)]
        public ActionResult UnAuthorized()
        {
            return View();
        }

        [AxAuthorize(StateType = StateType.Ignore)]
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var password = AxEncoding.CalculateChecksumMd5(model.Password);
            var log = new LoginLog
            {
                AppVersion = "1.0",
                Browser = UserData.GetBrowser(Request.UserAgent) == null ? Request.Browser.Browser : UserData.GetBrowser(Request.UserAgent),
                BrowserVersion = UserData.GetBrowser(Request.UserAgent) == null ? Request.Browser.Version : "14",
                InsertDateTime = DateTime.Now,
                IP = NetworkTools.GetIpAddress(),
                OS = UserData.GetClientOs(Request.UserAgent, Request.Browser.Platform),
                UserName = model.UserName.ToLower(),

                CreatorUserId = 1,
                MachineName = NetworkTools.GetMachineName()
            };

            var user = Service.GetAll(x => x.UserName.ToLower() == model.UserName.ToLower() && x.Password == password && !x.IsDeleted && x.IsActive).Select(x => new { x.Id, x.IsActive, x.UserName }).FirstOrDefault();
            if (user == null)
            {
                log.ValidSignIn = false;
                log.InvalidPassword = model.Password;
                _loginLogService.Add(log);
                ModelState.AddModelError("", Words.InvalidUsernamePassword);
                return View();
            }

            log.ValidSignIn = true;
            log.UserId = user.Id;
            _loginLogService.Add(log);


            var token = TokenProvider.CreateToken(user.Id, DeviceType.Web);
            var httpCookie = new HttpCookie("AxToken", token.access_token)
            {
                Expires = DateTime.Now.AddSeconds(token.expires_in),
                HttpOnly = true,
                SameSite = SameSiteMode.Strict
            };
            Response.Cookies.Add(httpCookie);
            //if (Session["UserPermissions"] == null)
            //    SetUserPermissionSession(user.Id);
            return Redirect(GetRedirectUrl(model.ReturnUrl));
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "Dashboard", new { SystemKey = "Basic" });
            }
            return returnUrl;
        }

    }


}