﻿using System.Web.Mvc;
using Framework.Entities;
using WebUi.Filters;

namespace WebUi.Controllers
{
    public class ReportPrintController : Controller
    {
        [AxAuthorize(AxOp.PersonListReports, true)]
        public ActionResult PersonListReport()
        {
            return View();
        }
    }
}