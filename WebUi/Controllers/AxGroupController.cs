﻿using System.Linq;
using System.Web.Mvc;
using Framework;
using Framework.Entities;
using Framework.Resources;
using Framework.Services;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Model.Entities;
using WebUi.Filters;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class AxGroupController : BaseController<AxGroup, BaseViewModel<AxGroup>>
    {
        // GET: AxGroup
        public AxGroupController(IBaseService<AxGroup> service) : base(service)
        {
            Service = service;
        }

        [AxAuthorize(AxOp.GroupList, true)]
        public ActionResult List()
        {
            return View();
        }

        [AxAuthorize(ParentAxOp = AxOp.GroupList, StateType = StateType.CheckParent)]
        public JsonResult GetGroupList([DataSourceRequest] DataSourceRequest request)
        {
            var groupList = Service.GetAll().OrderByDescending(x => x.Id).Select(x => new
            {
                x.Id,
                x.GroupName,
                x.Description,
                x.InsertDateTime,

            });
            return Json(groupList.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [AxAuthorize(AxOp.GroupItem)]
        public ActionResult Item(int id = 0)
        {
            var viewModel = new AxGroupViewModel();
            var model = Service.Find(id);
            if (model != null)
            {
                viewModel.Entity = model;
            }
            return View(viewModel);
        }

        [AxAuthorize(AxOp.GroupInsert)]
        public override ActionResult Insert(BaseViewModel<AxGroup> item)
        {
            var entity = item.Entity;
            Service.Add(entity);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessInsert,Id=entity.Id });
        }

        [AxAuthorize(AxOp.GroupUpdate)]
        public override ActionResult Update(BaseViewModel<AxGroup> item)
        {
            var entity = item.Entity;
            Service.Update(entity);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessEdit });
        }

        [AxAuthorize(AxOp.GroupDelete)]
        public override ActionResult Delete(int id)
        {
            return base.Delete(id);
        }
    }
}