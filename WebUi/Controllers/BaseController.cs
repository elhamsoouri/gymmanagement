﻿using System.Web.Mvc;
using AutoMapper;
using Framework;
using Framework.Resources;
using Framework.Services;
using Model.Entities;
using WebUi.Models;

namespace WebUi.Controllers
{
    public class BaseController<TEntity, TViewModel> : Controller where TEntity : BaseEntity where TViewModel : BaseViewModel<TEntity>
    {
        protected IBaseService<TEntity> Service { get; set; }

        public BaseController(IBaseService<TEntity> service)
        {
            Service = service;
        }
        private TViewModel ConvertToViewModel(TEntity entity)
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap(typeof(TEntity), typeof(TViewModel));
            }).CreateMapper();
            var viewmodel = mapper.Map<TEntity, TViewModel>(entity);
            return viewmodel;
        }
        [HttpPost]
        public virtual ActionResult Insert(TViewModel item)
        {
            var entity = item.Entity;
            Service.Add(entity);
            return Json(new ApiResult());
        }
        [HttpPost]
        public virtual ActionResult Update(TViewModel item)
        {
            var entity = item.Entity;
            Service.Update(entity);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessEdit });
        }
        [HttpPost]
        public virtual ActionResult Delete(int id)
        {
            Service.Delete(id);
            return Json(new ApiResult { Type = ApiResultType.Success, Msg = Words.SuccessDelete });
        }
    }
}