﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Mvc;
using Framework;
using Framework.Repositories;
using Framework.Tools;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Model.Entities;

namespace WebUi.Provider
{
    public class OAuthAppProvider : OAuthAuthorizationServerProvider
    {
        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                var username = context.UserName;
                var password = context.Password;
                var userD = Cryptography.Decrypt(username);
                var passD = Cryptography.Decrypt(password);
                var passM = AxEncoding.CalculateChecksumMd5(passD);
                var repository = DependencyResolver.Current.GetService<IBaseRepository<User>>();
                var user = repository.GetAll(x => x.UserName.ToLower() == userD.ToLower() && x.Password == passM).FirstOrDefault();
                if (user != null)
                {
                    var claims = new List<Claim> { new Claim(ClaimTypes.Name, user.Id.ToString()) };
                    var oAutIdentity = new ClaimsIdentity(claims, Startup.OAuthOptions.AuthenticationType);
                    context.Validated(new AuthenticationTicket(oAutIdentity, new AuthenticationProperties()));
                }
                else
                {
                    context.SetError("401", "Error");
                }
            });
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            if (context.ClientId == null)
            {
                context.Validated();
            }
            return Task.FromResult<object>(null);
        }
    }

    public enum TokenType
    {
        None = 0,
        Anonymous = 1,
        Signed = 2
    }

    public static class TokenProvider
    {
        public static Token CreateToken(int userId, DeviceType appType)
        {
            var identity = new ClaimsIdentity(Startup.OAuthOptions.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, userId.ToString()));

            AuthenticationTicket ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
            var currentUtc = new SystemClock().UtcNow;
            ticket.Properties.IssuedUtc = currentUtc;
            ticket.Properties.ExpiresUtc = currentUtc.Add(appType == DeviceType.Phone ? TimeSpan.FromHours(8760) : TimeSpan.FromMinutes(20));

            var obj = new Token
            {
                token_type = "bearer",
                access_token = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket),
                expires_in = appType == DeviceType.Phone ? TimeSpan.FromHours(8760).TotalSeconds : TimeSpan.FromHours(1).TotalSeconds,
                type = TokenType.Signed,
                created_at = ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),
                //NickName = string.IsNullOrWhiteSpace(user.NickName) ? user.UserName : user.NickName
            };
            return obj;
        }

        public static int GetUserId(IIdentity identity)
        {
            var userId = identity.Name;
            if (!string.IsNullOrWhiteSpace(userId))
                return int.Parse(userId);
            return 0;
        }
        // ReSharper disable once MethodOverloadWithOptionalParameter
        public static int? GetUserId(IIdentity identity, bool isnull = true)
        {
            var userId = identity.Name;
            if (!string.IsNullOrWhiteSpace(userId))
                return int.Parse(userId);
            return null;
        }
        //public static RoleType GetUserRole(IIdentity identity)
        //{
        //    var claimsIdentity = (ClaimsIdentity)identity;
        //    IEnumerable<Claim> claims = claimsIdentity.Claims;
        //    var isAdmin = bool.Parse(claims.ElementAt(2).Value);
        //    var role = isAdmin ? RoleType.Admin : RoleType.User;
        //    return role;
        //}

    }

    public class Token
    {

        public string token_type { get; set; }
        public string access_token { get; set; }
        public double expires_in { get; set; }
        public long created_at { get; set; }
        public string NickName { get; set; }
        public TokenType type { get; set; }
        public int userId { get; set; }
    }
}