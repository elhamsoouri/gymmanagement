﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Entities;

namespace WebUi.Models
{
    public class FileViewModel : BaseViewModel<File>
    {
        public string Size { get; set; }
    }
}