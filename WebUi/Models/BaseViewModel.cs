﻿using System;
using System.ComponentModel.DataAnnotations;
using Model.Entities;

namespace WebUi.Models
{
    public class BaseViewModel<T>
    {
        public BaseViewModel()
        {
            Entity = (T)Activator.CreateInstance(typeof(T));
        }
        public T Entity { get; set; }
        public int Id { get; set; }
        public int CreatorUserId { get; set; }
        public int? EditorUserId { get; set; }
        public DateTime InsertDateTime { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
        public byte[] RowVersion { get; set; }
    }
}