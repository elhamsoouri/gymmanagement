﻿using System.ComponentModel.DataAnnotations;
using Model.Entities;

namespace WebUi.Models
{
    public class AxGroupViewModel : BaseViewModel<AxGroup>
    {
        [Display(Name = "نام کاربر")]
        public int UserId { get; set; }
    }
}