﻿using FluentValidation;
using FluentValidation.Attributes;
using Model.Entities;

namespace WebUi.Models
{
    [Validator(typeof(LoginViewModelValidator))]
    public class LoginViewModel : BaseViewModel<User>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RePassword { get; set; }
        public string OldPassword { get; set; }
        public bool Remember { get; set; }
        public string ReturnUrl { get; set; }
    }

    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        public LoginViewModelValidator()
        {
            RuleFor(i => i.UserName).NotEmpty().WithMessage("نام کاربری را وارد کنید");
            RuleFor(i => i.Password).NotEmpty().WithMessage("رمز عبور را وارد کنید");
        }
    }
}