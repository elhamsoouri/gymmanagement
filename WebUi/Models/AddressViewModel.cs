﻿using System.ComponentModel;
using Model.Entities;

namespace WebUi.Models
{
    public class AddressViewModel : BaseViewModel<Address>
    {
        [DisplayName(" موقعیت جغرافیایی")]
        public virtual string GeoPosTitle { get; set; }
    }
}