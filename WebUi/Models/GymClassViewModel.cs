﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Model.Entities;
using Model.Enum;

namespace WebUi.Models
{
    public class GymClassViewModel : BaseViewModel<GymClass>
    {

        [Display(Name = "نام کلاس")]
        [Required]
        public string ClassName { get; set; }
        [Display(Name = "کد کلاس")]
        [Required]
        public string ClassNo { get; set; }
        [Display(Name = "نام مربی")]
        [Required]
        public string CoachName { get; set; }//personId
        public int CoachId { get; set; }//personId
        [Display(Name = "ظرفیت")]
        public int Capacity { get; set; }
        [Display(Name = "شروع کلاس")]
        [Required]
        public string BeginDate { get; set; }
        [Display(Name = "پایان کلاس")]
        [Required]
        public string ExpireDate { get; set; }
        [Display(Name = "ساعت شروع")]
        [Required]
        public string StartTime { get; set; }
        [Display(Name = "ساعت پایان")]
        [Required]
        public string EndTime { get; set; }
        [Display(Name = "شهریه کلاس")]
        public long Price { get; set; }
        [Display(Name = "روزهای کلاس")]
        [Required]
        public List<DropDownModel> ClassDatesLoad { get; set; }
        public ClassDatesEnum ClassDates { get; set; }
        public string ClassDatesList { get; set; }

        [Display(Name = "دستمزد مربی به ازائ هر شاگرد")]
        public long PricePerStudent { get; set; }
        public string ClassTime => StartTime + " - " + EndTime;
    }

    public class DropDownModel
    {

        public string Name { get; set; }
        public int Id { get; set; }
    }
}