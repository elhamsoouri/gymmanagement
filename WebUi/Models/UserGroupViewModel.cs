﻿using System.ComponentModel.DataAnnotations;
using Model.Entities;

namespace WebUi.Models
{
    public class UserGroupViewModel : BaseViewModel<UserGroup>
    {
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "نام و نام خانوادگی")]
        public string FullName { get; set; }
        [Display(Name = "شماره پرسنلی")]
        public string PersonalId { get; set; }
    }
}