﻿using System;
using Model.Entities;

namespace WebUi.Models
{
    public class PersonViewModel : BaseViewModel<Person>
    {
        public string FullName
        {
            get
            {
                return Entity.FirstName + " " + Entity.LastName;
            }
            set { }
        }
        public int PersonPictureId { get; set; }
        public Guid ImageId { get; set; }
        public byte[] Image { get; set; }
        public string SystemName
        {
            get
            {
                return "General";
            }
        }
        public string Birthday { get; set; }
    }
}