﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using FluentValidation;
using FluentValidation.Mvc;
using Framework;
using Framework.Entities;
using Framework.Repositories;
using Framework.Services;
using Framework.Tools;
using Framework.UnitOfWork;
using Framework.Validator;
using Model.Entities;
using Repository;
using WebUi.Filters;


namespace WebUi
{
    public class Global : HttpApplication
    {
        public static IContainer Container { get; set; }
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            FluentValidationModelValidatorProvider.Configure();

            // Autofac Configuration : 
            var builder = new ContainerBuilder();

            builder.RegisterType<DataContext>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IBaseRepository<>)).InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(BaseService<>)).As(typeof(IBaseService<>)).InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(typeof(User).Assembly).Where(t => t.IsClosedTypeOf(typeof(IValidator<>))).AsImplementedInterfaces();
            builder.RegisterGeneric(typeof(AxValidator<>)).As(typeof(IValidator<>)).InstancePerLifetimeScope();
            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            var container = builder.Build();
            Container = container;
            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container)); //Set the MVC DependencyResolver
            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);//Set the WebApi DependencyResolver

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(Container);


            var asm = Assembly.GetAssembly(typeof(WebApiConfig));

            var controllersActionList = asm.GetTypes()
                .Where(type => typeof(Controller).IsAssignableFrom(type))
                .SelectMany(type => type.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any())
                .Select(x => new AxModel { Controller = x.DeclaringType?.Name, Action = x.Name, ReturnType = x.ReturnType.Name, AxAuthorizeAttribute = x.GetCustomAttributes().Where(t => t.GetType().Name == "AxAuthorizeAttribute").Select(t => t as AxAuthorizeAttribute).FirstOrDefault(), Attributes = x.GetCustomAttributes().Select(a => new { Name = a.GetType().Name.Replace("Attribute", ""), Value = GetValue(a) }) })
                .OrderBy(x => x.Controller).ThenBy(x => x.Action).ToList();

            var menuRepository = DependencyResolver.Current.GetService<IBaseRepository<Menu>>();
            var axModels = controllersActionList.Where(x => !x.Controller.Contains("BaseController")).ToList();
            axModels.ForEach(x =>
                x.Url = "/" + GetSystemName(x.AxAuthorizeAttribute, x.Controller, x.Action) + "/"
                        + x.Controller.Replace("Controller", "") + "/"
                        + x.Action);

            foreach (var item in axModels)
            {

                if (item.AxAuthorizeAttribute.StateType == StateType.Authorized)
                {
                    var lst = GetAxMenus(item.AxAuthorizeAttribute.AxOp, item.Url);
                    var key = item.AxAuthorizeAttribute.AxOp.GetAxKey();
                    int? parentId = null;
                    foreach (var menu in lst)
                    {
                        var dbMenu = menuRepository.FirstOrDefault(x => x.Key == menu.Key);

                        if (menu.Key == key)
                            menu.ShowInMenu = item.AxAuthorizeAttribute.ShowInMenu;

                        if (dbMenu == null)
                        {
                            menu.ParentId = parentId;
                            menuRepository.Add(menu);
                        }

                        parentId = dbMenu?.Id ?? menu.Id;
                    }
                }
            }
        }

        private string GetSystemName(AxAuthorizeAttribute axAuthorizeAttribute, string controller, string action)
        {
            if (axAuthorizeAttribute == null)
                throw new Exception($"Deer programmer you forget set AxOpAttribute in {controller} in {action}");
            return axAuthorizeAttribute.AxOp.GetAxSystem();
        }

        public List<Menu> GetAxMenus(AxOp axOp, string itemUrl)
        {
            var axDisplay = axOp.GetAttribute<AxDisplay>();
            var node = axDisplay;
            var me = axOp;
            var lst = new List<Menu>();
            while (true)
            {
                var menu = new Menu
                {
                    Active = true,
                    CreatorUserId = 1,
                    InsertDateTime = DateTime.Now,
                    AxOp = me,
                    OrderId = node.Order,
                    Title = node.Title,
                    Key = me.GetAxKey()
                };
                lst.Insert(0, menu);

                if (node.Parent == AxOp.None)
                {
                    lst[lst.Count - 1].Url = itemUrl;
                    return lst;
                }

                me = node.Parent;
                node = node.Parent.GetAttribute<AxDisplay>();
            }
        }

        private StateType GetValue(Attribute attribute)
        {
            if (attribute is AxAuthorizeAttribute authorizeAttribute)
                return authorizeAttribute.StateType;
            return StateType.Authorized;
        }
    }

    internal class AxModel
    {
        public AxAuthorizeAttribute AxAuthorizeAttribute { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public object Attributes { get; set; }
        public string ReturnType { get; set; }
        public string Url { get; set; }
    }
}