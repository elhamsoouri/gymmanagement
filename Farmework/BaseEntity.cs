﻿using System;
using System.ComponentModel.DataAnnotations;
using Model.Entities;

namespace Framework
{
    public abstract class BaseEntity : IBaseEntity
    {
        [Display(Name = "شناسه")]
        public int Id { get; set; }
        [Display(Name = "تاریخ ایجاد")]
        public DateTime InsertDateTime { get; set; }
        [Display(Name = "تاریخ ویرایش")]
        public DateTime? ModifiedDateTime { get; set; }
        public int CreatorUserId { get; set; }
        public int? EditorUserId { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
