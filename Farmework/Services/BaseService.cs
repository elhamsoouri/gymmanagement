﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FluentValidation;
using Framework.Repositories;

namespace Framework.Services
{
    public class BaseService<T> : IBaseService<T> where T : BaseEntity
    {
        private readonly IBaseRepository<T> _baseRepository;
        protected IValidator<T> Validator;
        public BaseService(IBaseRepository<T> baseRepository, IValidator<T> validator)
        {
            _baseRepository = baseRepository;
            Validator = validator;
        }

        protected void Validate(T entity)
        {
            var result = Validator.Validate(entity);
            if (!result.IsValid)
            {
                //var x = result.Errors.Select(i => i.ErrorMessage).Aggregate((s1, s2) => $"{s1} \r\n  {s2}");
                throw new ValidationException(result.Errors);
            }
        }

        public IQueryable<T> GetAll()
        {
            return _baseRepository.GetAll();
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _baseRepository.GetAll(predicate);
        }

        public T Find(int id)
        {
            return _baseRepository.Find(id);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return _baseRepository.FirstOrDefault(predicate);
        }

        public T FirstOrDefault()
        {
            return _baseRepository.FirstOrDefault();
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return _baseRepository.Count(predicate);
        }

        public void Add(T entity)
        {
            entity.InsertDateTime = DateTime.Now;
            entity.CreatorUserId = 1;
            Validate(entity);
            _baseRepository.Add(entity);
        }

        public void Update(T entity)
        {
            entity.ModifiedDateTime = DateTime.Now;
            Validate(entity);
            _baseRepository.Update(entity);
        }

        public void Delete(int id)
        {
            _baseRepository.Delete(id);
        }

        public void Delete(T entity)
        {
            _baseRepository.Delete(entity);
        }

        public void BulkAdd(T entity)
        {
            Validate(entity);
            _baseRepository.BulkAdd(entity);
        }

        public void BulkUpdate(T entity)
        {
            Validate(entity);
            _baseRepository.BulkUpdate(entity);
        }

        public void BulkDelete(int id)
        {
            _baseRepository.BulkDelete(id);

        }

        public void BulkDelete(T entity)
        {
            _baseRepository.BulkDelete(entity);
        }

        public int SaveChange()
        {
            return _baseRepository.SaveChange();
        }
    }
}
