﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Framework.Services
{
    public interface IBaseService<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);
        T Find(int id);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        T FirstOrDefault();
        int Count(Expression<Func<T, bool>> predicate);
        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
        void BulkAdd(T entity);
        void BulkUpdate(T entity);
        void BulkDelete(int id);
        void BulkDelete(T entity);
        int SaveChange();
    }
}