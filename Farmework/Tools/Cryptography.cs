﻿using System;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace Framework.Tools
{
    public class Cryptography
    {
        private const string SecretKey = "Zd2B5wxkwOB1f8R3"; //16 char secret key

        public static string Base64Decoder(string input)
        {
            string ps = "";
            if (string.IsNullOrWhiteSpace(input)) return "";
            if (!string.IsNullOrWhiteSpace(input))
            {
                byte[] arrTemp;
                try
                {
                    arrTemp = Convert.FromBase64String(input);
                }
                catch
                {
                    try
                    {
                        arrTemp = Convert.FromBase64String(input + "=");
                    }
                    catch
                    {
                        arrTemp = Convert.FromBase64String(input + "==");
                    }
                }

                if (arrTemp.Length > 0)
                    ps = Encoding.UTF8.GetString(arrTemp);
            }

            return ps;
        }

        public static string Base64Encoder(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return "";
            var tmp = Encoding.UTF8.GetBytes(input);
            string ps = Convert.ToBase64String(tmp);
            return ps;
        }

        public static string CalculateChecksumMd5(string inputString)
        {
            var md5 = new MD5CryptoServiceProvider();
            var hashbytes = md5.ComputeHash(Encoding.UTF8.GetBytes(inputString));
            var hashstring = "";
            foreach (var hashbyte in hashbytes)
                hashstring += hashbyte.ToString("x2");
            return hashstring;
        }


        public static string Decrypt(string textToDecrypt)
        {
            return Decrypt(textToDecrypt, SecretKey);
        }

        private static string Decrypt(string textToDecrypt, string key)
        {
            var rijndaelCipher = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 0x80,
                BlockSize = 0x80
            };

            byte[] encryptedData = Convert.FromBase64String(textToDecrypt);
            byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
            byte[] keyBytes = new byte[0x10];
            int len = pwdBytes.Length;
            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }

            Array.Copy(pwdBytes, keyBytes, len);
            rijndaelCipher.Key = keyBytes;
            rijndaelCipher.IV = keyBytes;
            byte[] plainText = rijndaelCipher.CreateDecryptor()
                .TransformFinalBlock(encryptedData, 0, encryptedData.Length);
            return Encoding.UTF8.GetString(plainText);
        }

        public static string Encrypt(string textToEncrypt)
        {
            return Encrypt(textToEncrypt, SecretKey);
        }

        private static string Encrypt(string textToEncrypt, string key)
        {
            var rijndaelCipher = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = 0x80,
                BlockSize = 0x80
            };

            byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
            byte[] keyBytes = new byte[0x10];
            int len = pwdBytes.Length;
            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }

            Array.Copy(pwdBytes, keyBytes, len);
            rijndaelCipher.Key = keyBytes;
            rijndaelCipher.IV = keyBytes;
            var transform = rijndaelCipher.CreateEncryptor();
            byte[] plainText = Encoding.UTF8.GetBytes(textToEncrypt);
            return Convert.ToBase64String(transform.TransformFinalBlock(plainText, 0, plainText.Length));
        }

        public static T Decrypt<T>(string strEnc)
        {
            if (strEnc != null)
            {
                var entityStr = Decrypt(strEnc);
                var entity = JsonConvert.DeserializeObject<T>(entityStr);
                return entity;
            }
            return default(T);
        }

    }
}

