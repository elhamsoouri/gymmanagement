﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Framework.Tools
{
   public static class ReflectionTools
    {
        public static PropertyInfo GetPropertyInfo<TSource, TProperty>(
            TSource source,
            Expression<Func<TSource, TProperty>> propertyLambda)
        {
            Type type = typeof(TSource);

            MemberExpression member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException($"Expression '{propertyLambda}' refers to a method, not a property.");

            PropertyInfo propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException($"Expression '{propertyLambda}' refers to a field, not a property.");

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType ?? throw new InvalidOperationException()))
                throw new ArgumentException($"Expression '{propertyLambda}' refers to a property that is not from type {type}.");

            return propInfo;
        }

        public static string GetName<TSource, TField>(Expression<Func<TSource, TField>> field)
        {
            return (field.Body as MemberExpression ?? (MemberExpression) ((UnaryExpression)field.Body).Operand).Member.Name;
        }

    }
}
