﻿using System.Security.Cryptography;

namespace Framework.Tools
{
    public class AxEncoding
    {
        public static string CalculateChecksumMd5(string inputString)
        {
            if (string.IsNullOrWhiteSpace(inputString))
                return null;
            var md5 = new MD5CryptoServiceProvider();
            var hashBytes = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(inputString));
            var hashString = "";
            foreach (var hashByte in hashBytes)
                hashString += hashByte.ToString("x2");
            return hashString;
        }
    }
}
