﻿using System;
using System.Linq;
using System.Net;
using System.Web;

namespace Framework.Tools
{
    public class NetworkTools
    {
        public static string GetIpAddress()
        {
            var ipAddressString = HttpContext.Current.Request.UserHostAddress;

            if (ipAddressString == null)
                return null;

            IPAddress.TryParse(ipAddressString, out var ipAddress);

            // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
            // This usually only happens when the browser is on the same machine as the server.
            if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
            {
                try
                {
                    Dns.GetHostEntry(ipAddressString);
                    ipAddress = Dns.GetHostEntry(ipAddress).AddressList.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                }
                catch (Exception)
                {
                    return "unkownan";
                }
            }
            if (ipAddress != null && !string.IsNullOrWhiteSpace(ipAddress.ToString()))
                return ipAddress.ToString();
            else return "unkownan";
        }
        public static string GetMachineName()
        {
            var ipAddressString = HttpContext.Current.Request.UserHostAddress;
            var myIp = IPAddress.Parse(ipAddressString ?? throw new InvalidOperationException());
            try
            {
                var host = Dns.GetHostEntry(ipAddressString);
                var compName = host.HostName.Split('.').ToList();
                return compName.First();
            }
            catch (Exception)
            {
                return "unkownan";
            }

        }


    }
}
