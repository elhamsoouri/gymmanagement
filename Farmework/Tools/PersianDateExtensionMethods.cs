﻿using System;
using System.Globalization;
using System.Reflection;

namespace Framework.Tools
{
    public static class PersianDateExtensionMethods
    {
        private static CultureInfo _culture;

        public static CultureInfo GetPersianCulture()
        {
            if (_culture == null)
            {
                _culture = new CultureInfo("fa-IR");
                DateTimeFormatInfo formatInfo = _culture.DateTimeFormat;
                formatInfo.AbbreviatedDayNames = new[] { "ی", "د", "س", "چ", "پ", "ج", "ش" };
                formatInfo.DayNames = new[] { "یکشنبه", "دوشنبه", "سه شنبه", "چهار شنبه", "پنجشنبه", "جمعه", "شنبه" };
                var monthNames = new[]
                {
                    "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن",
                    "اسفند",
                    ""
                };
                formatInfo.AbbreviatedMonthNames =
                    formatInfo.MonthNames =
                    formatInfo.MonthGenitiveNames = formatInfo.AbbreviatedMonthGenitiveNames = monthNames;
                formatInfo.AMDesignator = "ق.ظ";
                formatInfo.PMDesignator = "ب.ظ";
                formatInfo.ShortDatePattern = "yyyy/MM/dd";
                formatInfo.LongDatePattern = "dddd, dd MMMM,yyyy";
                formatInfo.FirstDayOfWeek = DayOfWeek.Saturday;
                System.Globalization.Calendar cal = new PersianCalendar();

                FieldInfo fieldInfo = _culture.GetType().GetField("calendar", BindingFlags.NonPublic | BindingFlags.Instance);
                if (fieldInfo != null)
                    fieldInfo.SetValue(_culture, cal);

                FieldInfo info = formatInfo.GetType().GetField("calendar", BindingFlags.NonPublic | BindingFlags.Instance);
                if (info != null)
                    info.SetValue(formatInfo, cal);

                _culture.NumberFormat.NumberDecimalSeparator = "/";
                _culture.NumberFormat.DigitSubstitution = DigitShapes.NativeNational;
                _culture.NumberFormat.NumberNegativePattern = 0;
            }
            return _culture;
        }

        public static string ToPerDateTimeString(this DateTime date, string format)
        {
            if (string.IsNullOrWhiteSpace(format))
                format = "yyyy/MM/dd HH:mm:ss";
            return date.ToString(format, GetPersianCulture());
        }

        public static DateTime? GetMiladiDate(this string input)
        {
            try
            {
                var pd = new PersianCalendar();
                var str = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string[] arrTemp = str[0].Split(new[] { '-', '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
                if (arrTemp.Length == 3)
                {
                    return pd.ToDateTime(int.Parse(arrTemp[0]), int.Parse(arrTemp[1]), int.Parse(arrTemp[2]), 0, 0, 0, 0).Date;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static DateTime? GetMiladiDateTime(string input)
        {
            try
            {
                var pd = new PersianCalendar();
                var str = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string[] arrTemp = str[0].Split(new[] { '-', '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
                string[] arrTemp2 = str[1].Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (arrTemp.Length == 3 && arrTemp2.Length == 3)
                {
                    return pd.ToDateTime(int.Parse(arrTemp[0]), int.Parse(arrTemp[1]), int.Parse(arrTemp[2]), int.Parse(arrTemp2[0]), int.Parse(arrTemp2[1]), int.Parse(arrTemp2[2]), 0);
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
