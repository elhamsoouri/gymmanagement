﻿namespace Framework
{
    public enum LogType
    {
        Error = 1,
        Warning = 2
    }
    public enum Platform
    {
        WebService = 1,
        WebApp = 2,
        AndroidApp = 3,
        IosApp = 4,
        WpApp = 5

    }

    public enum DeviceType
    {
        Phone = 1,
        Tablet = 2,
        Web = 3
    }

    public enum ResponseStatusType
    {
        Confirm = 1,
        Reject = 2,
        AdRegistered = 3,
        Offer = 4,
        ForAdmin = 5,
        UserRegister = 6,
        SecurityAlert = 7
    }
    public enum StateType
    {
        Authorized,
        Ignore,
        CheckParent
    }
}
