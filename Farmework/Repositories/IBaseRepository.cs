﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Framework.Repositories
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);
        int Count(Expression<Func<T, bool>> predicate);
        T FirstOrDefault(Expression<Func<T, bool>> predicate);
        T FirstOrDefault();
        T Find(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
        void BulkAdd(T entity);
        void BulkUpdate(T entity);
        void BulkDelete(int id);
        void BulkDelete(T entity);
        int SaveChange();
    }
}