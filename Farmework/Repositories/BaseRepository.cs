﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Framework.UnitOfWork;

namespace Framework.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        private readonly IUnitOfWork _uow;
        private readonly IDbSet<T> _entities;
        public BaseRepository(IUnitOfWork context)
        {
            _uow = context;
            _entities = context.Set<T>();
        }
        public IQueryable<T> GetAll()
        {
            return _entities;
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _entities.Where(predicate);
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return _entities.Count(predicate);
        }

        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return _entities.FirstOrDefault(predicate);
        }

        public T FirstOrDefault()
        {
            return _entities.FirstOrDefault();
        }

        public T Find(int id)
        {
            return _entities.Find(id);
        }

        public void Add(T entity)
        {
            _entities.Add(entity);
            _uow.SaveChanges();
        }

        public void Update(T entity)
        {
            var localRow = _uow.Set<T>().AsNoTracking().FirstOrDefault(x => x.Id == entity.Id);

            if (localRow == null)
                throw new Exception($"Entity with Id = {entity.Id} not Exist in Table= {typeof(T).Name}");

            entity.InsertDateTime = localRow.InsertDateTime;
            entity.CreatorUserId = localRow.CreatorUserId;
            _uow.Entry(entity).State = EntityState.Modified;
            _uow.SaveChanges();
        }

        public void Delete(int id)
        {
            var entity = Find(id);
            _entities.Remove(entity);
            _uow.SaveChanges();
        }

        public void Delete(T entity)
        {
            _entities.Remove(entity);
            _uow.SaveChanges();
        }

        public void BulkAdd(T entity)
        {
            _entities.Add(entity);
        }

        public void BulkUpdate(T entity)
        {
            _uow.Entry(entity).State = EntityState.Modified;
        }

        public void BulkDelete(int id)
        {
            var entity = Find(id);
            _entities.Remove(entity);
        }

        public void BulkDelete(T entity)
        {
            _entities.Remove(entity);
        }

        public int SaveChange()
        {
            return _uow.SaveChanges();
        }
    }
}
