﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model.Entities
{
    public interface IBaseEntity
    {
        int CreatorUserId { get; set; }
        int? EditorUserId { get; set; }
        DateTime InsertDateTime { get; set; }
        DateTime? ModifiedDateTime { get; set; }
        [Timestamp]
        byte[] RowVersion { get; set; }
    }
}