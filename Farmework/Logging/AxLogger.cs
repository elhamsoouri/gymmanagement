﻿
using NLog;
using System;

namespace Framework.Logging
{
    public static class AxLogger
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static void Info(string message, params object[] args)
        {
            Logger.Info(message, args);
        }

        public static void Info(Exception exception, string message, params object[] args)
        {
            Logger.Info(exception, message, args);
        }

        public static void Debug(string message, params object[] args)
        {
            Logger.Debug(message, args);
        }

        public static void Debug(Exception exception, string message, params object[] args)
        {
            Logger.Debug(exception, message, args);
        }

        public static void Error(string message, params object[] args)
        {
            Logger.Error(message, args);
        }

        public static void Error(Exception exception, string message, params object[] args)
        {
            Logger.Error(exception, message, args);
        }

        public static void Log(LogLevel logLevel, Exception exception, string message, params object[] args)
        {
            Logger.Log(logLevel, exception, message, args);
        }
        public static void Log(LogLevel logLevel, string message, params object[] args)
        {
            Logger.Log(logLevel, message, args);
        }
    }
}
