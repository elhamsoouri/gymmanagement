﻿CREATE TABLE AxTechDB.dbo.Logs (
  Id int IDENTITY,
  Logged datetime NOT NULL,
  Level nvarchar(max) NOT NULL,
  Message nvarchar(max) NOT NULL,
  UserName nvarchar(max) NULL,
  ServerName nvarchar(max) NULL,
  Port nvarchar(max) NULL,
  Url nvarchar(max) NULL,
  ServerAddress nvarchar(max) NULL,
  RemoteAddress nvarchar(max) NULL,
  Logger nvarchar(max) NULL,
  Callsite nvarchar(max) NULL,
  Exception nvarchar(max) NULL,
  CreatorUserId int NOT NULL,
  EditorUserId int NULL,
  InsertDateTime datetime NOT NULL,
  ModifiedDateTime datetime NULL,
  RowVersion timestamp,
  CONSTRAINT [PK_FMK.Logs] PRIMARY KEY CLUSTERED (Id)
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO