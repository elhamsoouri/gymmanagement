namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class longPropertyChangedToNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "NationalCode", c => c.Long());
            AlterColumn("dbo.People", "IdentityNumber", c => c.Long());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "IdentityNumber", c => c.Long(nullable: false));
            AlterColumn("dbo.People", "NationalCode", c => c.Long(nullable: false));
        }
    }
}
