namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileTypeChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Files", "FileType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Files", "FileType", c => c.String());
        }
    }
}
