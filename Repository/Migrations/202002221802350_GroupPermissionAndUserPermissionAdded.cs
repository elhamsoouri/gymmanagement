namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GroupPermissionAndUserPermissionAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserPermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Access = c.Boolean(nullable: false),
                        MenuId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Menus", t => t.MenuId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.MenuId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.GroupPermissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupId = c.Int(nullable: false),
                        Access = c.Boolean(nullable: false),
                        MenuId = c.Int(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AxGroups", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.Menus", t => t.MenuId, cascadeDelete: true)
                .Index(t => t.GroupId)
                .Index(t => t.MenuId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.GroupPermissions", "MenuId", "dbo.Menus");
            DropForeignKey("dbo.GroupPermissions", "GroupId", "dbo.AxGroups");
            DropForeignKey("dbo.UserPermissions", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserPermissions", "MenuId", "dbo.Menus");
            DropIndex("dbo.GroupPermissions", new[] { "MenuId" });
            DropIndex("dbo.GroupPermissions", new[] { "GroupId" });
            DropIndex("dbo.UserPermissions", new[] { "UserId" });
            DropIndex("dbo.UserPermissions", new[] { "MenuId" });
            DropTable("dbo.GroupPermissions");
            DropTable("dbo.UserPermissions");
        }
    }
}
