namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MenuChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Menus", "AxOp", c => c.Int(nullable: false));
            AddColumn("dbo.Menus", "Key", c => c.String());
            AddColumn("dbo.Menus", "ShowInMenu", c => c.Boolean(nullable: false));
            DropColumn("dbo.Menus", "OpCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Menus", "OpCode", c => c.Long(nullable: false));
            DropColumn("dbo.Menus", "ShowInMenu");
            DropColumn("dbo.Menus", "Key");
            DropColumn("dbo.Menus", "AxOp");
        }
    }
}
