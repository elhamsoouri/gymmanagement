namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MenuAndSystemChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Menus", "SystemId", "dbo.AxSystems");
            DropIndex("dbo.Menus", new[] { "SystemId" });
            AddColumn("dbo.Menus", "OpCode", c => c.Long(nullable: false));
            DropColumn("dbo.Menus", "SystemId");
            DropTable("dbo.AxSystems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AxSystems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Key = c.String(),
                        Active = c.Boolean(nullable: false),
                        Icon = c.String(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Menus", "SystemId", c => c.Int());
            DropColumn("dbo.Menus", "OpCode");
            CreateIndex("dbo.Menus", "SystemId");
            AddForeignKey("dbo.Menus", "SystemId", "dbo.AxSystems", "Id");
        }
    }
}
