namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PersonIdChangedToNullableInUserTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "PersonId", "dbo.People");
            DropIndex("dbo.Users", new[] { "PersonId" });
            AlterColumn("dbo.Users", "PersonId", c => c.Int());
            CreateIndex("dbo.Users", "PersonId");
            AddForeignKey("dbo.Users", "PersonId", "dbo.People", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "PersonId", "dbo.People");
            DropIndex("dbo.Users", new[] { "PersonId" });
            AlterColumn("dbo.Users", "PersonId", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "PersonId");
            AddForeignKey("dbo.Users", "PersonId", "dbo.People", "Id", cascadeDelete: true);
        }
    }
}
