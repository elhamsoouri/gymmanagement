namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LongToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.People", "NationalCode", c => c.String());
            AlterColumn("dbo.People", "IdentityNumber", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.People", "IdentityNumber", c => c.Long());
            AlterColumn("dbo.People", "NationalCode", c => c.Long());
        }
    }
}
