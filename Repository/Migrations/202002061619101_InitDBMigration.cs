namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDBMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GeoPosId = c.Int(nullable: false),
                        Mobile = c.String(),
                        Email = c.String(),
                        Telephone = c.String(),
                        Fax = c.String(),
                        IsMainAddress = c.Boolean(nullable: false),
                        PersonId = c.Int(nullable: false),
                        PostalCode = c.String(),
                        PostalBox = c.String(),
                        LatinAddress = c.String(),
                        AddressContent = c.String(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeoPos", t => t.GeoPosId, cascadeDelete: true)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.GeoPosId)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.GeoPos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ParentId = c.Int(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeoPos", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        FatherName = c.String(),
                        Sex = c.Boolean(),
                        Birthday = c.DateTime(nullable: false),
                        NationalCode = c.String(),
                        IdentityNumber = c.String(),
                        IdentitySerial = c.String(),
                        LatinTitle = c.String(),
                        LatinName = c.String(),
                        LatinLastName = c.String(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GymClasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClassName = c.String(),
                        ClassNo = c.String(),
                        Price = c.Long(nullable: false),
                        ClassDates = c.Int(nullable: false),
                        StartTime = c.String(),
                        EndTime = c.String(),
                        BeginDate = c.DateTime(nullable: false),
                        ExpireDate = c.DateTime(nullable: false),
                        Capacity = c.Int(nullable: false),
                        PricePerStudent = c.Long(nullable: false),
                        CoachId = c.Int(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CoachId, cascadeDelete: true)
                .Index(t => t.CoachId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Password = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        PersonId = c.Int(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FileContent = c.Binary(),
                        FileType = c.String(),
                        Description = c.String(),
                        PersonId = c.Int(nullable: false),
                        Size = c.Int(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId);
            
            CreateTable(
                "dbo.LoginLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        InvalidPassword = c.String(),
                        Browser = c.String(),
                        BrowserVersion = c.String(),
                        OS = c.String(),
                        UserId = c.Int(),
                        AppVersion = c.String(),
                        IP = c.String(),
                        MachineName = c.String(),
                        ValidSignIn = c.Boolean(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ParentId = c.Int(),
                        Url = c.String(),
                        ShowInPopUp = c.Boolean(nullable: false),
                        CssClass = c.String(),
                        Active = c.Boolean(nullable: false),
                        OrderId = c.Int(nullable: false),
                        SystemId = c.Int(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AxSystems", t => t.SystemId)
                .ForeignKey("dbo.Menus", t => t.ParentId)
                .Index(t => t.ParentId)
                .Index(t => t.SystemId);
            
            CreateTable(
                "dbo.AxSystems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Key = c.String(),
                        Active = c.Boolean(nullable: false),
                        Icon = c.String(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Menus", "ParentId", "dbo.Menus");
            DropForeignKey("dbo.Menus", "SystemId", "dbo.AxSystems");
            DropForeignKey("dbo.LoginLogs", "UserId", "dbo.Users");
            DropForeignKey("dbo.Files", "PersonId", "dbo.People");
            DropForeignKey("dbo.GymClasses", "CoachId", "dbo.Users");
            DropForeignKey("dbo.Users", "PersonId", "dbo.People");
            DropForeignKey("dbo.Addresses", "PersonId", "dbo.People");
            DropForeignKey("dbo.Addresses", "GeoPosId", "dbo.GeoPos");
            DropForeignKey("dbo.GeoPos", "ParentId", "dbo.GeoPos");
            DropIndex("dbo.Menus", new[] { "SystemId" });
            DropIndex("dbo.Menus", new[] { "ParentId" });
            DropIndex("dbo.LoginLogs", new[] { "UserId" });
            DropIndex("dbo.Files", new[] { "PersonId" });
            DropIndex("dbo.Users", new[] { "PersonId" });
            DropIndex("dbo.GymClasses", new[] { "CoachId" });
            DropIndex("dbo.GeoPos", new[] { "ParentId" });
            DropIndex("dbo.Addresses", new[] { "PersonId" });
            DropIndex("dbo.Addresses", new[] { "GeoPosId" });
            DropTable("dbo.AxSystems");
            DropTable("dbo.Menus");
            DropTable("dbo.LoginLogs");
            DropTable("dbo.Files");
            DropTable("dbo.Users");
            DropTable("dbo.GymClasses");
            DropTable("dbo.People");
            DropTable("dbo.GeoPos");
            DropTable("dbo.Addresses");
        }
    }
}
