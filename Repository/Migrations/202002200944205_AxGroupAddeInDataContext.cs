namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AxGroupAddeInDataContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AxGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupName = c.String(),
                        Description = c.String(),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GroupId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        InsertDateTime = c.DateTime(nullable: false),
                        ModifiedDateTime = c.DateTime(),
                        CreatorUserId = c.Int(nullable: false),
                        EditorUserId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        AxGroup_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AxGroups", t => t.AxGroup_Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.AxGroup_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserGroups", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserGroups", "AxGroup_Id", "dbo.AxGroups");
            DropIndex("dbo.UserGroups", new[] { "AxGroup_Id" });
            DropIndex("dbo.UserGroups", new[] { "UserId" });
            DropTable("dbo.UserGroups");
            DropTable("dbo.AxGroups");
        }
    }
}
