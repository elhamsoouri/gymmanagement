namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PersonIdAddedtoPersonTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.People", "PersonalId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.People", "PersonalId");
        }
    }
}
