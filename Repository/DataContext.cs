﻿using System.Data.Entity;
using Framework.UnitOfWork;
using Model.Entities;
using Repository.Migrations;

namespace Repository
{
    public class DataContext : DbContext, IUnitOfWork
    {
        public DataContext() : base("DBConnectionString")
        {
            Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Configuration>());
            //Database.SetInitializer<DataContext>(null);
        }

        public DbSet<Menu> Menus { get; set; }
        public DbSet<GymClass> Classes { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<GeoPos> GeoPos { get; set; }
        public DbSet<LoginLog> LoginLogs { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Menu>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<GymClass>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<Person>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<File>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<Address>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<GeoPos>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<LoginLog>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<AxGroup>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<UserPermission>().Property(s => s.RowVersion).IsRowVersion();
            modelBuilder.Entity<GroupPermission>().Property(s => s.RowVersion).IsRowVersion();
        }
    }
}
