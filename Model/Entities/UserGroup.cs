﻿using Framework;

namespace Model.Entities
{
    public class UserGroup : BaseEntity
    {
        public int GroupId { get; set; }
        public virtual AxGroup AxGroup { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
