﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection.Emit;
using FluentValidation.Attributes;
using Framework;
using Framework.Entities;
using Framework.Validator;

namespace Model.Entities
{
    [Validator(typeof(AxValidator<Menu>))]
    public class Menu : BaseEntity
    {
        public string Title { get; set; }
        public int? ParentId { get; set; }
        public string Url { get; set; }
        public bool ShowInPopUp { get; set; }
        public string CssClass { get; set; }
        public bool Active { get; set; }
        public int OrderId { get; set; }
        public ICollection<Menu> Children { get; set; }
        [ForeignKey("ParentId")]
        public Menu Parent { get; set; }
        public AxOp AxOp { get; set; }
        public string Key { get; set; }
        public bool ShowInMenu { get; set; }
        public ICollection<UserPermission> UserPermissions { get; set; }
        public ICollection<GroupPermission> GroupPermissions { get; set; }
    }
}
