﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Mvc;
using FluentValidation;
using FluentValidation.Attributes;
using Framework;
using Framework.Repositories;
using Framework.Tools;

namespace Model.Entities
{
    [Validator(typeof(UserValidator))]
    public class User : BaseEntity
    {
        [DisplayName("نام کاربری")]
        public string UserName { get; set; }
        [DisplayName("رمز عبور")]
        public string Password { get; set; }
        [DisplayName("فعال")]
        public bool IsActive { get; set; } = true;
        [DisplayName("حذف شده")]
        public bool IsDeleted { get; set; }
        [DisplayName("نام شخص")]
        public int? PersonId { get; set; }
        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }
        //public virtual ICollection<UserPermission> UserPermission { get; set; }
        public virtual ICollection<UserGroup> GroupUsers { get; set; }
    }

    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.UserName).NotEmpty().WithMessage("^نام کاربری را وارد کنید");
            RuleFor(user => user.Password).NotEmpty().WithMessage("^پسورد را وارد کنید");
            RuleFor(user => user.Password).Must(HeavyPassword).WithMessage("^پسورد ایمن نیست");
            RuleFor(user => user.IsActive).NotEmpty().WithMessage("^وضعیت را وارد کنید");
            RuleFor(user => user.PersonId).Must((obj, personId) => CheckOldAccount(personId, obj.Id)).WithMessage("این شخص کاربری دارد^");
            RuleFor(user => user.UserName).Must((obj, userName) => UniqUserName(userName, obj.Id)).WithMessage("نام کاربری تکراری است^");
        }
        public bool HeavyPassword(string password)
        {

            PasswordScore passwordStrengthScore = PasswordAdvisor.CheckStrength(password);

            switch (passwordStrengthScore)
            {
                case PasswordScore.Blank:
                    return false;
                case PasswordScore.VeryWeak:
                    return false;
                case PasswordScore.Weak:
                    return false;
                case PasswordScore.Medium:
                    return true;
                case PasswordScore.Strong:
                    return true;
                case PasswordScore.VeryStrong:
                    return true;
                default: return false;
            }
        }
        private bool CheckOldAccount(int? perCompId, int id)
        {
            var repository = DependencyResolver.Current.GetService<IBaseRepository<User>>();
            var count = repository.GetAll(x => x.PersonId == perCompId && x.Id != id).Count();
            if (count == 0)
                return true;
            return false;
        }
        private bool UniqUserName(string userName, int id)
        {
            var repository = DependencyResolver.Current.GetService<IBaseRepository<User>>();
            var count = repository.GetAll(x => x.UserName == userName && x.Id != id).Count();
            if (!string.IsNullOrWhiteSpace(userName) && count == 0)
                return true;
            return false;
        }
    }
}
