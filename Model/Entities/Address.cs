﻿using System.ComponentModel;
using FluentValidation;
using FluentValidation.Attributes;
using Framework;

namespace Model.Entities
{
    [Validator(typeof(AddressValidation))]
    public class Address : BaseEntity
    {
        [DisplayName("موقعیت جغرافیایی")]
        public virtual int GeoPosId { get; set; }
        public virtual GeoPos GeoPos { get; set; }
        [DisplayName("شماره موبایل")]
        public string Mobile { get; set; }
        [DisplayName("ایمیل")]
        public string Email { get; set; }
        [DisplayName("شماره تلفن")]
        public string Telephone { get; set; }
        [DisplayName("شماره فکس")]
        public string Fax { get; set; }
        [DisplayName(" آدرس اصلی")]
        public bool IsMainAddress { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        [DisplayName(" کد پستی")]
        public string PostalCode { get; set; }
        [DisplayName(" صندوق پستی")]
        public string PostalBox { get; set; }
        [DisplayName(" آدرس به لاتین")]
        public string LatinAddress { get; set; }
        [DisplayName(" آدرس")]
        public string AddressContent { get; set; }
    }

    public class AddressValidation : AbstractValidator<Address>
    {
        public AddressValidation()
        {
            RuleFor(user => user.GeoPosId).NotEmpty().WithMessage("لطفا موقعیت جغرافیایی را وارد نمایید");
            RuleFor(user => user.AddressContent).NotEmpty().WithMessage("لطفا آدرس را وارد نمایید");
        }
    }
}
