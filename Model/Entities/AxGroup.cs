﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation;
using FluentValidation.Attributes;
using Framework;

namespace Model.Entities
{
    [Validator(typeof(GroupValidator))]
    public class AxGroup : BaseEntity
    {
        [Display(Name = "نام گروه")]
        public string GroupName { get; set; }
        [Display(Name = "شرح")]
        public string Description { get; set; }
        public virtual ICollection<UserGroup> GroupUsers { get; set; }
        //public virtual ICollection<GroupPermission> GroupPermission { get; set; }
    }

    public class GroupValidator : AbstractValidator<AxGroup>
    {
        public GroupValidator()
        {
            RuleFor(group => group.GroupName).NotEmpty().WithMessage("^نام کاربری را وارد کنید");
            RuleFor(group => group.Description).NotEmpty().WithMessage("^پسورد را وارد کنید");
        }
    }
}
