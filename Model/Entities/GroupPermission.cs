﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using Framework;

namespace Model.Entities
{
    public class GroupPermission : BaseEntity
    {
        public int GroupId { get; set; }
        [ForeignKey("GroupId")]
        public virtual AxGroup AxGroup { get; set; }
        public bool Access { get; set; }
        public int MenuId { get; set; }
        [ForeignKey("MenuId")]
        public virtual Menu Menu { get; set; }

    }
}
