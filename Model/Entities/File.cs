﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Framework;
using Model.Enum;

namespace Model.Entities
{
    public class File : BaseEntity
    {
        public string Title { get; set; }
        public byte[] FileContent { get; set; }
        public FileEnum FileType { get; set; }
        public string Description { get; set; }
        public int PersonId { get; set; }
        [ForeignKey("PersonId")]
        public Person Person { get; set; }
        public int Size { get; set; }
        public string FileName { get; set; }

    }
}