﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Framework;

namespace Model.Entities
{
    public class LoginLog : BaseEntity
    {
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "رمز وارد شده")]
        public string InvalidPassword { get; set; }
        [Display(Name = "مرورگر")]
        public string Browser { get; set; }
        [Display(Name = "نسخه مرورگر")]
        public string BrowserVersion { get; set; }
        [Display(Name = "سیستم عامل")]
        public string OS { get; set; }
        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        [Display(Name = "نسخه نرم افزار")]
        public string AppVersion { get; set; }
        [Display(Name = "آی پی")]
        public string IP { get; set; }
        [Display(Name = "نام دستگاه")]
        public string MachineName { get; set; }
        [Display(Name = "ورود موفق")]
        public bool ValidSignIn { get; set; }
    }
}
