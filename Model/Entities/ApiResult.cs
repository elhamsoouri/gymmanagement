﻿using System.ComponentModel.DataAnnotations;
using Framework.Tools;

namespace Model.Entities
{
    public class ApiResult<T> : ApiResult where T : class
    {
        public T Entity { get; set; }
    }

    public class ApiResult
    {

        public int Id { get; set; }

        private string _msg;
        public string Msg
        {
            get => string.IsNullOrWhiteSpace(_msg) ? Type.GetDisplayName() : _msg;
            set => _msg = value;
        }

        public ApiResultType Type { get; set; } = ApiResultType.Success;
        public string Temp { get; set; }
    }

    public enum ApiResultType
    {
        [Display(Name = "عملیات با موفقیت انجام شد")]
        Success = 1,
        [Display(Name = "خطای مدیریت نشده رخ داده است")]
        Error = 2,
        [Display(Name = "عملیات با هشدار انجام شد")]
        Warning = 3,
        [Display(Name = "عملیات با داده اضافی")]
        Information = 4,
        [Display(Name = "ورودی های ثبت شده را کنترل نمائید")]
        ValidationError = 5
    }
}
