﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Framework;

namespace Model.Entities
{
    public class GeoPos : BaseEntity
    {
        public string Title { get; set; }
        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual GeoPos Parent { get; set; }
        public virtual ICollection<GeoPos> Children { get; set; }
    }
}
