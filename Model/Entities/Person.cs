﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FluentValidation;
using FluentValidation.Attributes;
using Framework;
using Framework.Repositories;

namespace Model.Entities
{
    [Validator(typeof(PersonValidator))]
    public class Person : BaseEntity
    {
        public Person()
        {
            Addresses = new List<Address>();
        }
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("نام")]
        public string FirstName { get; set; }
        [DisplayName("نام خانوادگی")]
        public string LastName { get; set; }
        [DisplayName("نام و نام خانوادگی")]
        public string FullName => FirstName + " " + LastName;
        [DisplayName("نام پدر")]
        public string FatherName { get; set; }
        [DisplayName("جنسیت")]
        public bool? Sex { get; set; }
        [DisplayName("تاریخ تولد")]
        public DateTime? Birthday { get; set; }
        [DisplayName("شماره پرسنلی")]
        public string PersonalId { get; set; }
        [DisplayName("کد ملی")]
        public string NationalCode { get; set; }
        [DisplayName("شماره شناسنامه")]
        public string IdentityNumber { get; set; }
        [DisplayName("سریال شناسنامه")]
        public string IdentitySerial { get; set; }
        [DisplayName("نام لاتین عنوان")]
        public string LatinTitle { get; set; }
        [DisplayName("نام لاتین")]
        public string LatinName { get; set; }
        [DisplayName("نام خانوادگی لاتین")]
        public string LatinLastName { get; set; }
        public ICollection<Address> Addresses { get; set; }
    }

    public class PersonValidator : AbstractValidator<Person>
    {
        private string ResultMessage { get; set; } = "پیغام خطا";
        public PersonValidator()
        {
            RuleFor(user => user.LastName).NotEmpty().WithMessage("لطفا نام خانوادگی را وارد نمایید");
            RuleFor(user => user.FirstName).NotEmpty().WithMessage("لطفا نام را وارد نمایید");
            RuleFor(x => x.NationalCode).Must(CheckNationalCode).WithMessage(x => ResultMessage);
        }
        private bool CheckNationalCode(Person person, string arg)
        {
            var repository = DependencyResolver.Current.GetService<IBaseRepository<Person>>();
            if (arg != null && arg.Length != 10)
            {
                ResultMessage = " کد ملی صحیح نمی باشد";
                return false;
            }

            if (person.NationalCode != null)
            {
                var nationalCode = !repository.GetAll(x => x.NationalCode == person.NationalCode && x.Id != person.Id).Any();
                if (!nationalCode)
                {
                    ResultMessage = " کد ملی تکراری می باشد ";
                    return false;
                }
            }
            return true;
        }
    }
}
