﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FluentValidation;
using FluentValidation.Attributes;
using Framework;
using Framework.Repositories;
using Model.Enum;

namespace Model.Entities
{
    [Validator(typeof(GymClassValidator))]
    public class GymClass : BaseEntity
    {
        [DisplayName("شماره کلاس")]
        public string ClassName { get; set; }
        public string ClassNo { get; set; }
        [NotMapped]
        public string Title { get; }
        public long Price { get; set; }
        public ClassDatesEnum ClassDates { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public int Capacity { get; set; }
        public long PricePerStudent { get; set; }
        public int CoachId { get; set; }//personId
        public User Coach { get; set; }
    }

    public class GymClassValidator : AbstractValidator<GymClass>
    {
        private readonly IBaseRepository<GymClass> _repository;
        public GymClassValidator()
        {
            _repository = DependencyResolver.Current.GetService<IBaseRepository<GymClass>>();
            RuleFor(x => x.ClassName).NotEmpty().WithMessage("لطفا نام کلاس را وارد کنید");
            RuleFor(x => x.ClassNo).NotEmpty().WithMessage("لطفا شماره کلاس را وارد کنید");
            RuleFor(x => x.ClassNo).Must((obj, arg) => UniqueClassNo(obj)).WithMessage("این شماره کلاس قبلا ثبت شده است");
            RuleFor(x => x.Price).NotEmpty().WithMessage("لطفا شهریه کلاس را وارد کنید");
            RuleFor(x => x.ClassDates).NotEmpty().WithMessage("لطفا روزهای کلاس را مشخص کنید");
            RuleFor(x => x.StartTime).NotEmpty().WithMessage("لطفا ساعت شروع کلاس را وارد کنید");
            RuleFor(x => x.EndTime).NotEmpty().WithMessage("لطفا ساعت پایان کلاس را وارد کنید");
            RuleFor(x => x.BeginDate).Must((obj, arg) => CheckDateTime(obj, arg, "تاریخ شروع")).WithMessage(x => ResultMessage);
            RuleFor(x => x.ExpireDate).Must((obj, arg) => CheckDateTime(obj, arg, "تاریخ پایان")).WithMessage(x => ResultMessage);
            RuleFor(x => x.Capacity).NotEmpty().WithMessage("لطفا ظرفیت کلاس را وارد کنید");
            RuleFor(x => x.CoachId).NotEmpty().WithMessage("لطفا نام مربی را مشخص کنید");
            RuleFor(x => x.EndTime).GreaterThan(y => y.StartTime).WithMessage("ساعت پایان کلاس نباید زودتر از ساعت شروع باشد ");
            RuleFor(x => x.ExpireDate).GreaterThan(y => y.BeginDate).WithMessage("تاریخ پایان کلاس نباید زودتر از تاریخ شروع باشد ");
            RuleFor(x => x.PricePerStudent).GreaterThan(0).NotEmpty().WithMessage("دستمزد مربی به ازائ هر شاگرد را مشخص کنید");
        }

        private string ResultMessage { get; set; } = "پیغام خطا";
        private bool UniqueClassNo(GymClass gymClass)
        {
            return !_repository.GetAll(x => x.ClassNo == gymClass.ClassNo && x.Id != gymClass.Id).Any();
        }
        private bool CheckDateTime(GymClass gymClass, DateTime? arg, string fieldName)
        {
            DateTime temp = default;
            if (arg != null)
            {
                DateTime dt = DateTime.Parse(arg?.ToString(CultureInfo.InvariantCulture));
                if (temp == dt)
                {
                    ResultMessage = $" {fieldName} صحیح نمی باشد";
                    return false;
                }
            }
            return true;
        }
    }

}
