﻿using System.ComponentModel.DataAnnotations.Schema;
using Framework;

namespace Model.Entities
{
    public class UserPermission : BaseEntity
    {
        public bool Access { get; set; }
        public int MenuId { get; set; }
        [ForeignKey("MenuId")]
        public virtual Menu Menu { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
