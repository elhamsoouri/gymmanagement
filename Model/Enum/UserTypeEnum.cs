﻿using System.ComponentModel.DataAnnotations;

namespace Model.Enum
{
    public enum UserTypeEnum
    {
        [Display(Name = "راهبر سیستم")]
        Admin = 1,
        [Display(Name = "کاربر سیستم")]
        SystemUser = 2,
        [Display(Name = "مربی")]
        Coach = 3
    }
}