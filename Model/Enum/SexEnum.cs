﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Model.Enum
{
    public enum SexEnum
    {
        [Display(Name = "مونث")]
        Female = 100,
        [Display(Name = "مذکر")]
        Male = 200

    }
}