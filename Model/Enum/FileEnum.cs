﻿using System.ComponentModel.DataAnnotations;

namespace Model.Enum
{
    public enum FileEnum
    {
        [Display(Name = "عکس")]
        PersonPicture,
        [Display(Name = "مدارک")]
        PersonDocument
    }
}